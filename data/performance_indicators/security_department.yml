- name: MTTR (Mean-Time-To-Remediation) for severity 1, 2, and 3 security vulnerabilities over the past 30 days
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The MTTR metric is an indicator of our efficiency in remediating security vulnerabilities, whether they are reported through HackerOne bug bounty program (or other external means, such as security@gitlab.com emails) or internally-reported. The average days to close issues in the GitLab project (project_id = '278964')that are have the label `security` and severity 1, severity 2, or severity 3; this excludes issues with variation of the security label (e.g. `security review`) and severity 4 issues. Issues that are not yet closed are excluded from this analysis. This means historical data can change as older issues are closed and are introduced to the analysis. The average age in days threshold is set at the daily level. For Security purposes, please view this chart directly in Sisense.
  target: <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Time to remediate</a>
  org: Security Department
  is_key: true
  health:
    level: 1
    reasons:
    - MTTR metrics for severity 2 and 3 vulnerabilities are showing they need attention due to them being outside of our current SLOs listed in the <a href="https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security Handbook</a>. Severity 2 vulnerabilities have been missing the target SLO since November 2018 and severity 3 vulnerabilities missing since September 2019.    
  urls:
  - https://app.periscopedata.com/app/gitlab/641782/Appsec-KPIs?widget=11288778&udv=0
  - https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues

- name: Security Engineer On-Call Page Volume
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the volume and severity of paged incidents
    to the Security Engineer On-Call. For Security purposes, please view this chart directly in Sisense.
  target: Number of pages/month does not exceed +50% of monthly average of the last 12 months for 3 consecutive months
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - 
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9217413&udv=0

- name: Security Incidents by Category
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric groups security incidents by incident category. For Security purposes, please view this chart directly in Sisense.
  target: Number of security incidents in any category does not exceed +50% of the individual category's 3-month average.
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - 
  urls:
  - https://app.periscopedata.com/app/gitlab/592612/Security-KPIs?widget=9277541&udv=0

- name: Security Control Health
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: GCF security controls are conitnuously tested as parts of the Compliance
    team's continuous monitoring program, internal audits and external audits. A clear
    indicator of success is directly reflected in the control effectveness rating.
    Observations are a result of GCF security failure, indicating that the control
    is not implemented, designed effectively or is not operating effectively. These
    observations indicate a gap that requires remediation in order for the security
    control to be operating and audit ready.
  target: This will be determined in FY22 Q1 as part of GRC application onboarding
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - Control testing is officially executing in the third party GRC application as of February 1st.
  urls:
  - https://docs.google.com/spreadsheets/d/157htqF6TB_6vjt47zP9n2GN3lPGVPRVPPcnLu5ayPYk/edit?usp=sharing

- name: Security Impact on ARR
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The Field Security organization functions as a sales and customer enablement
    team therefore a clear indicator of success is directly reflected in the engagement
    of their services by Legal, Sales, TAMs and customers themselves.
  target: 60%
  org: Security Department
  is_key: true
  health:
    level: 3
    reasons:
    - This metric will be designed to report on ARR from February 1st on. The next KPI update will include a net new Sisense chart. January data is reported against IACV.
  urls:
    - https://app.periscopedata.com/app/gitlab/775136/WIP--Risk-and-Field-Security-KPIs?widget=10484456&udv=0
  sisense_data:
    dashboard: 775136
    chart: 10484456
    embed: v2 

- name: Cost of Abuse
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This metric is focused around the financial impact of abusive accounts and their activity. For Security purposes, please view this chart directly in Sisense.
  target: Cost of abuse is below $10K/Mo
  org: Security Department
  is_key: true
  health:
    level: 2
    reasons:
    - The first iteration of the Cost of Abuse dashboards, that are based on all blocked users, has been created. They display the volume and approximate cost, prior to discounting, of [CI/CD usage for private Namespaces](https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data?widget=10753102&udv=0) and [total storage](https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data?widget=10764641&udv=0) of blocked users, as well as the volume of accounts [blocked by the Trust and Safety team](https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data?widget=10739697&udv=0) per month. We've added two additional dashboards that display the volume of accounts blocked per [abuse category](https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data?widget=10787468&udv=0) as well as what the primary [product feature](https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data?widget=10787496&udv=0) being abused that resulted in the block. These dashboards are currently in a WIP status and will be reviewed for their accuracy as part of our FY22-Q1 OKR. The cost of abuse dashboards is an ongoing effort and as more granular usage data sources become available they will be added to create a more holistic view of the costs related to abusive activity on GitLab.com.   
  urls:
  - https://app.periscopedata.com/app/gitlab/780726/WIP:-Blocked-User-Usage-Data





- name: Average age of currently open bug bounty vulnerabilities by severity
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: The average age of currently open bug bounty vulnerabilities gives a
    health snapshot of how fast we are fixing the vulnerabilities that currently exist.
    This is important because it can help indicate what our future MTTR will look
    like and whether we are meeting our defined SLOs. The query is built by using
    the `ultimate_parent_id` of `9970` and is only for `open` `state` issues labelled
    with `security` and `hackerone`. The average age is measured in days, and the
    targets for each severity are defined in https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues.
  target: https://about.gitlab.com/handbook/engineering/security/#severity-and-priority-labels-on-security-issues
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Average age of currently open bug bounty vulnerabilities by severity gives a
      present health snapshot of our ability to fix vulnerabilites.

- name: HackerOne budget vs actual (with forecast of 'unknowns')
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We currently run a public bug bounty program through HackerOne, and
    this program has been largely successful - we get a lot of hacker engagement,
    and since the program went public, we have been able to resolve nearly 100 reported
    security vulnerabilities. The bounty spend is however, a budgeting forecast concern
    because of the unpredictability factor from month to month.
  target: TBD
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - H1 spend has been decreasing, even after the bump to "critical" and "high" findings.
      Projected spend for Q1 20 is $80k, compared to Q1 19's $180k.

- name: Security Discretionary Bonus Rate
  base_path: "/handbook/engineering/security/performance-indicators/index.html#security-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Security department
    every month.
  target: At or above 10%
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are trending below target.
  sisense_data:
    chart: 10941114
    dashboard: 592612
    embed: v2

- name: Security Hiring Actual vs Plan
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Security".
  target: 0.90
  org: Security Department
  health:
    level: 3
    reasons:
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8636794
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"

- name: Security Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market. For Security purposes, please view this chart directly in Sisense.
  target: Unknown until FY22 planning process
  org: Security Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently finance tells me when there is a problem, I’m not self-service.
    - Get the budget captured in a system
    - Chart budget vs. actual over time in periscope
  urls:
  - https://app.periscopedata.com/app/gitlab/633239/Security-Non-Headcount-BvAs

- name: Security Department New Hire Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Less than 0.66
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've been fluctuating  above and below the target recently, which is to be expected
      given the size of the department.
  sisense_data:
    chart: 9389232
    dashboard: 719541
    embed: v2

- name: Security Average Location Factor
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
    Data comes from BambooHR and is the average location factor of all team members
    in the Security department.
  target: Less than 0.70
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Security Operations having challenges pulling quality candidates in geo-diverse
      locations, in order to hit expectations and allow the team to grow, we will
      revisit our backlog US-based candidates.
  sisense_data:
    chart: 7864119
    dashboard: 592612
    embed: v2
  urls:
  - "/handbook/hiring/charts/security-department/"

- name: Security Overall Handbook Update Frequency Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/security` over time.
    time.
  target: 1
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons:
    - Following the holidays, we have seen a marked increase in engagement.
  sisense_data:
    chart: 10642322
    dashboard: 621064
    shared_dashboard: feac7198-86db-480b-9eae-c41cb479a209
    embed: v2

- name: Security Department Narrow MR Rate
  base_path: "/handbook/engineering/security/performance-indicators/"
  definition: This PI is in support of the engineering organization’s overall MR Rate objective however, this should not be considered a reflection of the performance or output of the Security Department whose work is primarily handbook driven.  Thus, there is no current target for Security Department MR Rate.
  target: 0
  org: Security Department
  is_key: false
  health:
    level: 3
    reasons: 
    - There is no current target for Security Department MR Rate.
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4446
  sisense_data:
    chart: 8934521
    dashboard: 686930
    shared_dashboard: ec910110-91bd-4a08-84aa-223bf6b3c772
    embed: v2
