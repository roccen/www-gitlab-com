- name: Support Satisfaction (SSAT)
  base_path: "/handbook/support/performance-indicators/"
  definition: A measure of how satisfied our customers' are with their interaction
    with the GitLab Support team. This is based on survey responses from customers
    after each ticket is solved by the Support team using a Good or Bad rating.
  target: At or above 95%
  org: Customer Support Department
  is_key: true
  health:
    level: 2
    reasons:
    - SSAT is very good (above 93%) but target is 95%.
    - Collecting feedback from dissatisfied customers to identify trends.
    - Support Managers review trends to create changes to workflows and/or training.
  sisense_data:
    chart: 5992548
    dashboard: 463858
    embed: v2
    border: false
- name: Service Level Agreement (SLA)
  base_path: "/handbook/support/performance-indicators/"
  definition: GitLab Support commits to an initial substantive response in a specified
    amount of time from the time the customer submits a ticket. The SLA for this first
    reply is based on each customer's Support Plan.
  target: At or above 95% to Priority Support SLAs
  org: Customer Support Department
  is_key: true
  health:
    level: 2
    reasons:
    - Inconsistent performance to a 95% target achievement.
    - Focused staffing to key time gaps with higher level of ticket SLA breaches.
    - Adjusting workflow to enable working with customers in their preferred timezone.
    - Working with the data team on understanding the inconsistent we see between
      our ZenDesk SLA data and Periscope via 2857
  urls:
  - https://about.gitlab.com/support/#service-level-agreement-sla-details
  - https://gitlab.com/gitlab-data/analytics/-/issues/2857
  sisense_data:
    chart: 6892851
    dashboard: 421422
    embed: v2
    border: false
- name: Customer Wait Times
  base_path: "/handbook/support/performance-indicators/"
  definition: The following KPI tracks the ratio between the median "Resolution Time" and the total median "Customer Wait Time" per ticket.
              "Resolution Time" is defined as the amount of time between the ticket creation and the last ticket communication.
              "Customer Wait Time" is defined as the total amount of time the ticket spends in the "Open" state over the tickets life cycle.
  target: 10% less than the median "Customer Wait Time" over a 6 months rolling average. 
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - TBD
- name: Customer Support Margin
  base_path: "/handbook/support/performance-indicators/"
  definition: Total Support headcount and non-headcount expenses as a percent of ARR.
  target: Headcount and non-headcount expenses to be at or below 10% of ARR
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - Tracked and reported monthly, and for the year we are under our margin target
      for spend.
    - Maintain focus on spend while iterating on needs for non-headcount expense requirements.
  sisense_data:
    chart: 6032346
    dashboard: 421422
    embed: v2
    border: false
- name: Support Hiring Actual vs Plan
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/"
  definition: Employees are in the division "Engineering" and department is "Support".
  target: At 136 Support members by Jan 31, 2022
  org: Customer Support Department
  is_key: false
  health:
    level: 3
    reasons:
    - Engineering is on plan. But we are lending some of our recruiters to sales for
      this quarter. And we just put in place a new "one star minimum" rule that might
      decrease offer volume.
    - 'Health: Monitor health closely'
  sisense_data:
    chart: 8636994
    dashboard: 421422
    embed: v2
  urls:
  - "/handbook/hiring/charts/customer-support/"
- name: Support Non-Headcount Plan vs Actuals
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Actuals below Plan
  org: Customer Support Department
  is_key: false
  health:
    level: 0
    reasons:
    - Currently finance tells me when there is a problem, I’m not self-service.
    - Get the budget captured in a system
    - Chart budget vs. actual over time in periscope
  urls:
  - https://app.periscopedata.com/app/gitlab/633248/Support-Non-Headcount-BvAs
- name: Support Average Location Factor
  base_path: "/handbook/support/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: At or below 0.6
  org: Customer Support Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are at our target of 0.58 exactly overall, but trending upward.
    - We need to get the target location factors in the charts.
    - It will probably be cleaner if we get this in periscope.
    - We need to set the target location factors on our vacancies and make sure recruiting
      is targeting the right areas of the globe on a per role basis.
  sisense_data:
    chart: 6119437
    dashboard: 421422
    embed: v2
  urls:
  - "/handbook/hiring/charts/customer-support/"
- name: Support Overall Handbook Update Frequency Rate
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching `/source/handbook/support/**` over time.
  target: 0.5
  org: Customer Support Department
  is_key: true
  health:
    level: 0
    reasons:
    - Unknown.
  sisense_data:
    chart: 10642313
    dashboard: 621060
    shared_dashboard: 185b8e19-a99e-4718-9aba-96cc5d3ea88b
    embed: v2
- name: Support Discretionary Bonus Rate
  base_path: "/handbook/support/performance-indicators/index.html#customer-support-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Customer Support department
    every month.
  target: At 10%
  org: Customer Support Department
  is_key: false
  health:
    level: 0
    reasons:
    - We currently track bonus percentages in aggregate, but there is no easy way
      to see the percentage for each individual department.
  urls:
  -
- name: Support Department Narrow MR Rate
  base_path: "/handbook/support/performance-indicators/"
  definition: Support Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is not directly a key performance indicator used to indicate productivity of our Support team members. Nonetheless, we want to track the average MR merged per team member to encourage updates to Documentation and Support 'fixes'. We currently count all members of the
    Support Department (Director, EMs, ICs) in the denominator because this is a team
    effort. The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts.
  target: At or above 5 MRs per Month
  org: Customer Support Department
  is_key: true
  health:
    level: 1
    reasons:
    - TBD
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/534
  sisense_data:
    chart: 8934827
    dashboard: 686943
    shared_dashboard: fc54cf54-e5f9-4708-9dfe-9e4c921ac79e?
    embed: v2
- name: Manager to customer support rep ratio
  base_path: "/handbook/support/performance-indicators/"
  definition: The Manager to IC Ratio is the ratio of individual contributors to one
    manager. This data comes from Bamboo HR.
  target: The target for this metric is at or below 10:1
  org: Customer Support Department
  is_key: true
  health:
    level: 3
    reasons:
    - While we have an overall ratio of less than 10:1 as a department at time of
      this evaluation, we have re-balanced the larger imbalance in EMEA with new management
      in place, and have plans for 3 additional managers to hire to accommodate growth
      in other teams.
    - Hire to plan for new managers.
    - Re-align ICs to new managers to restore balance.
  sisense_data:
    chart: 6119434
    dashboard: 421422
    embed: v2
    border: false
- name: Customer Support Department New Hire Average Location Factor
  base_path: "/handbook/support/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.61
  org: Customer Support Department
  is_key: false
  health:
    level: 3
    reasons:
    - We are under our target
  sisense_data:
    chart: 9389240
    dashboard: 719545
    embed: v2
