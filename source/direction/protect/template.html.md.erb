---
layout: secure_and_protect_direction
title: Product Stage Direction - Protect
description: "GitLab Protect includes all features related to protecting your applications and cloud infrastructure, identifying and cataloging threats. Learn more!"
canonical_path: "/direction/protect/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

<p align="center">
    <font size="+2">
        <b>Protecting cloud-native applications, services, and infrastructure</b>
    </font>
</p>

<%= partial("direction/protect/templates/overview") %>

<%= devops_diagram(["Protect"]) %>

## Stage Overview

The Protect Stage focuses on providing security visibility across the entire DevSecOps lifecycle as well as providing
monitoring and mitigation of attacks targeting your cloud-native environment.  Protect reduces overall security risk
by enabling you to be ready now for the cloud-native application stack and DevSecOps best practices of the future.
This is accomplished by:

* protecting your applications and services deployed within your cloud-native environment by leveraging context-aware technologies across the entire application stack
* monitoring for misuse within your GitLab deployment&mdash;including its associated runners&mdash;and alerting when anomalies in usage are detected

### Groups

The Protect Stage is made up of one group supporting the major categories focused on cloud-native security including:

* Container Security - Monitor and protect your cloud-native applications and services by leveraging context-aware knowledge to improve your overall security posture.

### Resourcing and Investment

The existing team members for the Protect Stage can be found in the links below:

* [Development](https://about.gitlab.com/company/team/?department=protect-section)
* [User Experience](https://about.gitlab.com/company/team/?department=protect-ux-team)
* [Product Management](https://about.gitlab.com/company/team/?department=protect-pm-team)
* [Quality Engineering](https://about.gitlab.com/company/team/?department=secure-enablement-qe-team)

## 3 Year Stage Themes

<%= partial("direction/protect/templates/themes") %>

## 3 Year Strategy
Effectively protecting applications running in production requires the following key software capabilities that will be developed over the next 3 years:
1. A streamlined workflow for triaging, investigating, and mitigating application attacks.  This workflow will be tightly integrated into the rest of the GitLab product.
1. Seamless two-way integrations with other security products to both allow GitLab to share alerts with other tools as well as to allow other tools to share information about attacks with GitLab.
1. A unified policy management experience for all security policies, spanning across GitLab&apos;s stages. The policy editor will be both versatile and easy to use.  By providing clear information about what policies are deployed, misconfigurations and accidental security holes can be avoided.
1. Analytics that save time and reduce errors by leveraging GitLab&apos;s knowledge of the application code to reduce false positives by tuning policies, effectively prioritizing alerts, and auto-suggesting security best practices.
1. In-depth introspection into the overhead, uptime, and performance of Protect technologies to ensure that security protections are always up and running without interfering with application performance.
1. Enterprise-grade user permissions, audit logging, change control, and reporting to ensure that the GitLab application itself is safe from threats.
1. Managed Security Service Provider (MSSP) support to allow partners to effectively and efficiently monitor the security posture of their clients&apos; applications.

## 1 Year Plan
Although the Protect stage has grandiose aspirations and a broad vision, the next 12 months of effort are entirely focused on building a foundation of security tools and capabilities that can be expanded later as the stage matures.

### High-level 12-month Roadmap

![Protect Overview](/images/direction/protect/protect-roadmap.png)

Please note that all roadmap items and timelines are subject to change

**Q1 FY&apos;22 - (February 2021 - April 2021)**
* [Initial MVC of a Project Level Alerts Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/3438) - [released in %13.9](/releases/2021/02/22/gitlab-13-9-released/#security-alert-dashboard-for-container-network-policy-alerts)
* [Follow-on Design Updates for Alert Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/5041)
* Release of initial MVC for [DAST Project-level Scan Execution Policies](https://gitlab.com/groups/gitlab-org/-/epics/4598)

**Q2 FY&apos;22 - (May 2021 - July 2021)**
* Continued improvements to [DAST Project-level Scan Execution Policies](https://gitlab.com/groups/gitlab-org/-/epics/4598)
* Default container scanning engine [Trivy to replace Clair](https://gitlab.com/groups/gitlab-org/-/epics/5398)
* [Ability to do vulnerability scans against running container images](https://gitlab.com/groups/gitlab-org/-/epics/3410)
* [Usage metric collection for Container Host Security](https://gitlab.com/gitlab-org/gitlab/-/issues/218800)
* Add support for Container Host Security through the GitLab Kubernetes Agent

**Q3 FY&apos;22 - (August 2021 - October 2021)**
* [Container Host Security Statistics](https://gitlab.com/groups/gitlab-org/-/epics/3377)
* [Ability to export Container Host Security logs to a SIEM](https://gitlab.com/groups/gitlab-org/-/epics/3391)
* [Automatic grouping/rollup of similar Alerts](https://gitlab.com/groups/gitlab-org/-/epics/3453)
* [Ability to scan images in GitLab&apos;s container registry on demand](https://gitlab.com/gitlab-org/gitlab/-/issues/37141)

**Q4 FY&apos;22 - (November 2021 - January 2022)**
* [Policy management UI for Container Host Security](https://gitlab.com/groups/gitlab-org/-/epics/3409)
* [Default policy pack for Container Host Security](https://gitlab.com/groups/gitlab-org/-/epics/3404)
* [Ability to view the Alert Dashboard at the group and instance levels](https://gitlab.com/groups/gitlab-org/-/epics/3455)

**Future / TBD**
* [Ability to view Container Host Security alerts in the GitLab UI](https://gitlab.com/groups/gitlab-org/-/epics/3412)
* [Ability to do malware scans on file systems of running containers](https://gitlab.com/groups/gitlab-org/-/epics/3411)
* [Ability to view Container Host Security logs in the GitLab UI](https://gitlab.com/groups/gitlab-org/-/epics/3413)
* [Ability to monitor the performance of Container Host Security](https://gitlab.com/groups/gitlab-org/-/epics/3414)
* [Active response options based on Container Host Security alerts](https://gitlab.com/groups/gitlab-org/-/epics/3376)

### What's Next for Protect
Key initiatives in this group over the next 12 months will include establishing a baseline level of functionality in the following areas:
* A standard, baseline UI for managing security policies across GitLab's stages
* A dashboard for viewing and responding to security alerts across GitLab's stages
* Improved container scanning functionality, including the ability to scan container images that are running in a production Kubernetes instance

Additionally, the Monitor:Health group is currently building out [on-call schedule management](https://gitlab.com/groups/gitlab-org/-/epics/3960). Once that feature set is complete, the Protect stage will be able to take advantage of it to automate the routing of security alerts to the right responder.

### What We're Not Doing
Although we will likely address many of these areas in the future (as described above in our [3 year strategy](/direction/protect/#3-year-strategy)), we are not planning to make progress on the following initiatives in the next 12 months:
* Adding support for applications running in serverless environments
* Adding support for applications running in non-Kubernetes containerized environments (such as OpenShift or Docker Swarm)
* Adding support for applications running in bare-metal / non-containerized environments
* Attempting to build our own Security Information and Event Management (SIEM) system
* Building analytics or algorithms to auto-tune or auto-recommend policy improvements

## Competitive Landscape

In our opinion, the Protect Stage aligns with the market that Gartner defines as the [Cloud Workload Protection Platform](https://www.gartner.com/en/documents/3983483) (CWPP) Market.

<p align="center">
    <img src="/images/direction/protect/gartner-cwpp.png" alt="Gartner CWPP Market">
</p>

*Gartner “[Market Guide for Cloud Workload Protection Platforms](https://www.gartner.com/en/documents/3983483),” Neil MacDonald, Tom Croll, 14 April 2020 (Gartner subscription required)*

We believe that GitLab currently aligns to the workload protection controls that Gartner defines in this market as follows:

1. **Hardening, configuration and vulnerability management** - This control is included in the charter for the [Container Scanning](/direction/protect/container-scanning/) category.  Functionality to scan containers running in production for vulnerabilities is [on the roadmap](https://gitlab.com/groups/gitlab-org/-/epics/3410).
1. **Identity-based segmentation and network visibility** - This control is the charter of the [Container Network Security](/direction/protect/container_network_security/) category.  This control was initially [released in GitLab 12.8](/releases/2020/02/22/gitlab-12-8-released/#network-policies-for-container-network-security) and has had several improvements since its release.
1. **System integrity assurance** - Gartner defines two parts to this control: preboot and postboot.  Currently GitLab does not have plans to address the preboot portion of this control other than through Container Scanning capabilities pre-deployment as part of GitLab&apos;s CI pipeline.  The postboot portion of the control is addressed by the [Container Host Security](/direction/protect/container_host_security/) category in functionality that was [released in GitLab 13.2](/releases/2020/07/22/gitlab-13-2-released/#container-host-monitoring-and-blocking).
1. **Application control/whitelisting** - GitLab is capable of addressing this control today through functionality that was [released in GitLab 13.2](/releases/2020/07/22/gitlab-13-2-released/#container-host-monitoring-and-blocking) as part of the control is addressed by the [Container Host Security](/direction/protect/container_host_security/) category.
1. **Exploit prevention/memory protection** - GitLab does not have plans to directly address this control at this time.  Some limited capabilities to address this control are available through GitLab&apos;s [integration with Pod Security Policies](https://docs.gitlab.com/ee/user/clusters/applications.html#using-podsecuritypolicy-in-your-deployments) which can limit which system calls that are available to a pod.
1. **Server Workload EDR, Behavioral Monitoring and Threat Detection/Response** - GitLab has some EDR capabilities as part of the [Container Host Security](/direction/protect/container_host_security/) category today that address this control.  Additional capabilities, including behavioral analytics, are an aspirational part of our long-term vision.
1. **HIPS with vulnerability shielding** - This control is jointly provided by three categories: [Container Host Security](/direction/protect/container_host_security/), [Container Network Security](/direction/protect/container_network_security/), and [Container Scanning](/direction/protect/container-scanning/).  Because GitLab provides IDS/IPS capabilities at both the network and host-based layers, there is some overlap and defense in depth, especially when this is coupled with regular vulnerability scans of containers running in production.
1. **Anti-malware scanning** - This control is included in the charter for the [Container Host Security](/direction/protect/container_host_security/) category.  Functionality to scan containers running in production for malware is [on the roadmap](https://gitlab.com/groups/gitlab-org/-/epics/3411).

## Key Performance Metrics

The following metrics are used to evaluate the success of the Protect stage:

* Overall **Performance Indicator**: This is the total number of activities analyzed across all groups in the Protect stage, including network traffic packets, process starts, and files accessed.
* **Stage Monthly Active Clusters**: This is the total number of clusters that have one or more Protect stage feature enabled
* **Stage Monthly Active Paid Clusters**: This is the total number of clusters belonging to paying customers that have one or more Protect stage feature enabled

## Target Audience
GitLab identifies who our DevSecOps application is built for utilizing the following categorization. We list our view of who we will support when in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the opportunities listed above, the Protect Stage has features that make it useful to the following personas today.
1. 🟨 [Security Operations Engineer](/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) / SecOps Teams
1. 🟨 [Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. ⬜️ [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) / DevOps Teams
1. ⬜️ Security Consultants

### Medium Term (1-2 years)
As we execute our [3 year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single DevSecOps application that enables SecOps to work collaboratively with DevOps and development to mitigate vulnerabilities in production environments.
1. 🟩 [Security Operations Engineer](/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) / SecOps Teams
1. 🟩 [Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
1. 🟨 [DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) / DevOps Teams
1. 🟨 Security Consultants

### Security Teams
Security teams and Security Operations teams are the primarily day-to-day users of Protect features.  Over time, GitLab aims to become the primary tool they use to protect, monitor, and manage the security of their production environments.  As most organizations have limited resources and/or security talent, we will strive to make the entire user experience as simple and easy to use as possible so as to minimize the skill set and number of individuals required.  Additionally, summary-level dashboards and reports will eventually be provided to allow for clear reporting up to executives.

Personas

* [Alex - Security Operations Engineer](/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer)
* [Sam - Security Analyst](/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst)
* [Cameron (Compliance Manager)](/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Delaney (Development Team Lead)](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Sasha (Software Developer)](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Devon (DevOps Engineer)](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* Internal Security Analyst (primarily responsible for insider threat monitoring)

### User Adoption Journey

Protect&apos;s Container Scanning and Security Orchestration categories are intended to be used by a wide range of personas, including Software Developers, Compliance Managers, DevOps Engineers, and Security Engineers.  To adopt these categories, the only prerequisite is for the user to adopt GitLab&apos;s CI pipelines.  For Container Scanning, users must also either use GitLab&apos;s Registry or have a Kubernetes instance connected to GitLab.

Protect&apos;s Container Network and Host Security categories are fundamentally different from all other GitLab stages because their primary target user sits outside the Development organization in a Security team that is most commonly within a larger IT department.  Consequently there are two primary paths to adopting these categories:
1. **Start with the security team; expand to the development organization later** - this user adoption journey involves the security team adopting Configure and then Protect to use GitLab to provide security for their production environment.  Initially another tool may even be used for their source code management, their CI/CD pipelines, and any other DevOps functions.  Once Protect is being used by the Security Team, additional user adoption then follows along the traditional [GitLab user adoption journey](/handbook/product/growth/#adoption-journey) model with the development organization typically starting with the Create stage.
1. **Start with the development organization; expand to the security team later** - this user adoption journey involves the customer first using GitLab for any one or more of our other stages, such as Secure, as part of their development workflow.  At any point along that adoption journey, the Security Team can then choose to start using Configure to connect to their production environment and then Protect to manage the security of their deployed applications.  The development organization does not need to have adopted Configure first.

## Pricing

<%= partial("direction/protect/templates/pricing") %>

<%= partial("direction/categories", :locals => { :stageKey => "protect" }) %>

## Other Top Level Features

The Security Orchestration category&apos;s features span across multiple stages in GitLab, including the Secure and Protect stages:

#### Alert Dashboard Vision
The alert dashboard is the central location for viewing and managing alerts.  The long-term plan is to aggregate alerts from all categories at the project, group, and instance levels and visualizing those alerts in both a list-view as well through explorable charts and graphs.  Alerts will be eventually be capable of being prioritized based on the severity of the alert (defined by the policy that generated the alert) as well as an analytical algorithm that scores the alert based on its level of risk.  The alert dashboard will also host the central workflow for taking action on alerts, including any auto-suggested remediation actions or recommended policy tuning changes.

#### Policy Management Vision
The policy management experience is the central location for policies across both the Secure and Protect stages.  Here users will be able to have the flexibility of managing policies directly in code or in a streamlined UI editor.  As part of the long-term vision for the policy management experience, users will be able to view a complete history of all changes and easily revert to a previous version.  A two-step approval process can optionally be enforced for all policy changes.  Eventually the policy management UI will be extended to provide visibility into the performance overhead of each policy.  Suggestions into policy adjustments that might help either reduce false positives or increase overall security coverage will be provided in this section as well.

<!-- Future item: **Performance Monitoring** -->

<!-- Future item: **Case Management** -->

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "protect" }) %>

<p align="center">
    <i><br />
    Last Reviewed: 2021-02-25<br />
    Last Updated: 2021-02-25
    </i>
</p>
