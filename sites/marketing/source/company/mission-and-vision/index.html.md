---
layout: markdown_page
title: "GitLab Mission and Vision"
description: "GitLab believe that all digital products should be open to contributions; from legal documents to movie scripts, and from websites to chip designs."
canonical_path: "/company/mission-and-vision/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Background

We believe in a world where **everyone can contribute**. We believe that all
digital products should be open to contributions; from legal documents to movie
scripts, and from websites to chip designs.

Allowing everyone to make a proposal is the core of what a DVCS
([Distributed Version Control System](https://en.wikipedia.org/wiki/Distributed_version_control))
such as Git enables. No invite needed: if you can see it, you can contribute.

We think that it is logical that our collaboration tools are a collaborative
work themselves. More than [3,000 people from the wider community](http://contributors.gitlab.com/) have
contributed to GitLab to make that a reality.

## Mission

It is GitLab's mission to change all creative work from read-only to
read-write so that **everyone can contribute**.

When **everyone can contribute**, consumers become contributors and we greatly
increase the rate of human progress.

## Big Hairy Audacious Goal

Our [BHAG](https://www.jimcollins.com/concepts/bhag.html) over
[the next 30 years](/company/cadence/#mission)
is to become
the most popular collaboration tool for knowledge workers in any industry. For
this, we need to make the DevOps lifecycle much more user friendly.

## Values

Our mission guides our path, and we live our [values](/handbook/values/) along this path.

## Vision

In summary, our vision is as follows:

GitLab Inc. develops great open source software to enable people to collaborate
in this way. GitLab is a [single application](/handbook/product/single-application/)
based on
[convention over configuration](/handbook/product/product-principles/#convention-over-configuration)
that everyone should be able to afford and adapt. With GitLab, **everyone can
contribute**.

## Goals

1. Ensure that **everyone can contribute** in the three ways outlined above.
2. Become the most used software for the software development lifecycle and collaboration on all digital content by following [the sequence below](#sequence).
3. Complete our [product vision](/direction/#vision) of a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/product-principles/#convention-over-configuration).
4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).
5. Remain independent so we can preserve our values. Since we took external investment, we need a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event). To remain independent, we want to become a [public company](/handbook/being-a-public-company/) instead of being acquired.

##  Everyone can contribute

Everyone can contribute to digital products with GitLab, to GitLab itself, and to our organization.
There are three ways you can Contribute,
1.   [Everyone can contribute with GitLab](/company/strategy/#contribute-with-gitlab)
1.   [Everyone can contribute to GitLab, the application](/company/mission-and-vision/#contribute-to-gitlab-application)
1.   [Everyone can contribute to GitLab, the company](/company/mission-and-vision/#contribute-to-gitlab-company)

### Everyone can contribute with GitLab
{:#contribute-with-gitlab}

To ensure that **everyone can contribute with GitLab** we allow anyone to create a proposal, at any time, without setup, and with confidence. Let's analyze that sentence a bit.
   - Anyone: Every person in the world should be able to afford great DevOps software. GitLab.com has free private repos and CI runners and GitLab CE is [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). But open source is more than a license, that is why we are [a good steward of GitLab CE](/company/stewardship/) and keep both GitLab CE and EE open to inspection, modifications, enhancements, and suggestions.
   - Create: It is a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/product-principles/#convention-over-configuration).
   - Proposal: With Git, if you can read it, you can fork it to create a proposal.
   - At any time: You can work concurrently with other people, without having to wait for permission or approval from others.
   - Without setup: You can make something without installing or configuring for hours with our web IDE and Auto DevOps.
   - With confidence: Reduce the risk of a flawed proposal with review apps, CI/CD, code quality, security scans, performance testing, and monitoring.

### Everyone can contribute to GitLab, the application
{:#contribute-to-gitlab-application}

We actively welcome contributors to ensure that **everyone can contribute to GitLab, the application**.
We do this by having quality code, tests, documentation, popular frameworks,
and offering a comprehensive [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
and a dedicated [GitLab Design System](https://design.gitlab.com/).
We use GitLab at GitLab Inc., we [dogfood](/handbook/product/product-processes/#dogfood-everything)
it and make it a tool we continue to love. We celebrate contributions by
recognizing a Most Valuable Person (MVP) every month.
We allow everyone to anticipate, propose, discuss, and contribute features by having everything on
a public issue tracker. We ship a new version every month so contributions
and feedback are visible fast. To contribute to open source software, people
must be empowered to learn programming.
That is why we sponsor initiatives such as Rails Girls.
   There are a few significant, but often overlooked, nuances of the **everyone can contribute to GitLab, the application** mantra:

   * While collaboration is a core value of GitLab, over collaborating tends to involve team members unnecessarily, leading to consensus-based decision making, and ultimately slowing the pace of improvement in the GitLab application. Consider [doing it yourself](/handbook/values/#collaboration), creating a merge request, and facilitating a discussion on the solution.
   * For valuable features in line with our product philosophy, that do not yet exist within the application, don't worry about UX having a world class design before shipping. While we must be good stewards of maintaining a quality product, we also believe in rapid iteration to add polish and depth after an [MVC](/handbook/product/product-principles/#the-minimal-viable-change-mvc) is created.
   * Prefer creating merge requests ahead of issues in order to suggest a tangible change to facilitate collaboration, driving conversation to the recommended implementation.
   * Contributors should feel free to create what they need in GitLab. If quality engineering requires charting features, for example, which would normally be implemented out of another team, they should feel empowered to prioritize their own time to focus on this aspect of the application.
   * GitLab maintainers, developers, and Product Managers should be viewed as coaches for contributions, independent of source. While there are contributions that may not get merged as-is (such as copy/paste of EE code into the CE code base or features that aren't aligned with product philosophy), the goal is to coach contributors to contribute in ways that are cohesive to the rest of the application.

   A group discussion reiterating the importance of everyone being able to contribute:
   <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/l374J98iOmk?t=675" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
   </figure>

### Everyone can contribute to GitLab, the company
{:#contribute-to-gitlab-company}

To ensure that **everyone can contribute to GitLab, the company** we have open business processes.
This allows all team members to suggest improvements to our
handbook. We hire remotely so everyone with an internet connection can come
work for us and be judged on results, not presence in an office. We offer
equal opportunity for every nationality. We are agnostic to location and
create more equality of opportunity in the world. We engage on Hacker News,
Twitter, and our blog post comments. And we strive to take decisions guided
by [our values](/handbook/values/).

#### Everyone can contribute to about.gitlab.com

We welcome all contributors in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) so that **everyone can contribute to about.gitlab.com**. GitLab uses about.gitlab.com to share our expertise with the world and believe we can build even greater levels of trust with contributions from our team and community. We strive to provide a great experience for our existing and new community members by reviewing changes and integrating the contributions into our regularly planned updates.

### Customer acceptance

We firmly adhere to laws [including trade compliance laws](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/#vii-compliance-with-all-laws-rules-and-regulations) in countries where we do business, and welcome everyone abiding by those legal restrictions to be customers of GitLab. In some circumstances, we may opt to not work with particular organizations, on a case-by-case basis. Some reasons we may choose not to work with certain entities include, but are not limited to:

1. Engaging in illegal, unlawful behavior.
1. Making derogatory statements or threats toward anyone in our community.
1. Encouraging violence or discrimination against legally protected groups.

This policy is in alignment with our mission, contributor and employee code-of-conduct and company values. Here are some links that may give you some background at how we arrived at this customer acceptance policy:

* Our mission is "everyone can contribute." This mission is in alignment with our open source roots and the [MIT license](https://en.wikipedia.org/wiki/MIT_License) our open source software is subject to. The MIT License is a free software license that allows users the freedom to run the program as they wish, for any purpose.

* GitLab has a [contributor code of conduct](/community/contribute/code-of-conduct/) for _how_ to contribute to GitLab, but there are no restrictions on _who_ can contribute to GitLab. We desire that everyone can contribute, as long as they abide by the code of conduct.

* GitLab has a set of values for how GitLab team members strive to conduct themselves. We don’t expect all companies to value collaboration, results, efficiency, diversity, inclusion and transparency in the same way we do. As an open company, “everyone can contribute” is our default and [transparency](/handbook/values/#transparency) is our check and balance. Transparency means our handbook, issues, merge requests and product roadmap are online for everyone to see and contribute to.

* Related topic: At GitLab, we want to avoid an environment where people feel alienated for their religious or political opinions. Therefore, we encourage GitLab team members to refrain from taking positions on specific [religious or political issues](/handbook/values/#religion-and-politics-at-work) in public company forums (such as on the GitLab Contribute stage or Slack channels) because it is easy to alienate people that may have a minority opinion. It is acceptable to bring up these topics in social contexts such as coffee chats and real-life meetups with other coworkers, but always be aware of cultural sensitivities, exercise your best judgement, and make sure you stay within the boundaries of our  [Code of Business Conduct & Ethics](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/). We always encourage [discussion and iteration](/handbook/values/#anyone-and-anything-can-be-questioned) on any company policy, including this one.

## Monitoring an evolving market

We'll also need to adapt with a changing market so that we meet customer needs. Netflix is a great example of this. Everyone knew that video on demand was the future. Netflix, however, started shipping DVDs over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.

Additionally, we need to ensure that our Platform is open. If a new, better version control technology enters the market, we will need to integrate it into our platform, as it is one component in an integrated DevOps product.

### Entering new markets

[GitLab](https://about.gitlab.com) has taken existing, fragmented software tooling markets, and by offering a consolidated offering instead, have created a new [blue ocean](https://www.blueoceanstrategy.com/what-is-blue-ocean-strategy/).

We would like to find more markets where we can repeat the same model.

The desirable characteristics of such markets fall into two stages: category consolidation and creation.  They are:

#### Category Consolidation

1. A set of customer needs that are currently served by multiple, independent software tools
1. Those tools may already integrate with each other or have the possibility of integration
1. Those tools operate in categories that are typically considered discreetly (e.g. with GitLab, SCM was one market, CI another)
1. There is no current 'winner' at consolidating this market, even if there are some attempts to combine some of the tool categories within said market
1. The users of the product would also be able to contribute to it e.g. with GitLab, the users are software developers, who can directly contribute code back to the product itself
1. When initially combined the categories form a consistent and desirable user flow which solves an overriding customer requirement
1. We can offer a consolidated toolchain more cost effectively for customers, than needing to purchase, run and integrate each tool separately
1. We can do so profitably for the company

#### Category Creation

1. By combining these categories together, a new overriding category, or market, gets created - the consolidated toolchain;
1. Further adjacent categories and/or markets can be added to deliver additional user value.  For example with GitLab, you could argue that the [Protect Category](/stages-devops-lifecycle/protect/) (as of October 2019) is an adjacent category/market to be added;
1. The sum of the categories combined should have desirable [CAGR](https://investinganswers.com/dictionary/c/compound-annual-growth-rate-cagr) such that entering the markets will mean entering those on a growth curve;
1. Not all of the individual categories need to be on the upward curve of the [technology adoption lifecycle](https://medium.com/@shivayogiks/what-is-technology-adoption-life-cycle-and-chasm-e07084e7991f) (TAL) - it is however necessary that there are enough future categories with high CAGR and early on in their adoption lifecycle - see example (very rough) diagram outlining this below:
(insert overlapping TALs);
1. The ideal overlap of the TALs is that the peaks of the curves are as close to each other as possible, so that when one TAL moves to Late Majority, the next TAL is still curving upwards.  This allows it to provide an organic growth opportunity for the company in the markets it is entering.

Our goal is to develop this model to be more quantifiable and formulaic, so that we can quickly and easily assess new opportunities.

### Risks

We acknowledge the risks to achieving our goals. We document them in our [biggest risks page](/handbook/leadership/biggest-risks/).

## Why is this page public?

Our strategy is completely public because transparency is one of our [values](/handbook/values/).
We're not afraid of sharing our strategy because, as Peter Drucker said,
"Strategy is a commodity, execution is an art."

