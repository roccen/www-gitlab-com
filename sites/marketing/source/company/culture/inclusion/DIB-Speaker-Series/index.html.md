---
layout: markdown_page
title: "Diversity, Inclusion & Belonging  Speaker Series"
description: "This page provides the videos and resources of past DIB Speaker Series events and an active list of upcoming DIB Speaker Series talks"
canonical_path: "/company/culture/inclusion/advisory-group-guide/"
---

## Diversity, Inclusion & Belonging Speaker Series 

This is a series of Speakers, Q&As, Talk and Workshops that the Diversity, Inclusion and Belonging Team, the DIB Advisory Board or Team Member Resource Groups organise on topics of DIB. If you have an idea for a speaker please reach out to a [DIB Team Member](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion-and-belonging-team)

### Upcoming Speakers 

- **Workshop Gitlab x Kinspace**: The importance and power of storytelling in building inclusive and intersectional workplaces - [Calendar link](https://calendar.google.com/event?action=TEMPLATE&tmeid=NjJubnA2OGdicW4yM2xwMGNoczQ2Ymt2MjQgbG1jbmFsbHlAZ2l0bGFiLmNvbQ&tmsrc=lmcnally%40gitlab.com)
- **Fireside and Q&A with [Shaun Christie-David](https://gitlab.slack.com/archives/CLLDY3L8P/p1617238241080700)** - Calendar link to follow


### Past Speakers and Events 

- **March 8th 2021 International Women's Day Event**: Kathryn Jacob, Sue Unerman and Mark Edwards. Authors of The Key to Transforming and Maintaining Diversity, Inclusion and Equality at Work

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/YNwpQITr8VM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

- **September 24th 2021 Sekou Kaalund - JP Morgan Chase**

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/4n8vRfvVyGE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
