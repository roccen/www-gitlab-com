| Priority | Description | Issue label(s) |
| ------ | ------ | ------ |
| 1* | <a href="/handbook/engineering/security/#severity-and-priority-labels-on-security-issues">Security fixes</a> | `security` |
| 2 | Data-loss prevention | `data loss` | 
| 3* | <a href="/handbook/engineering/performance/index.html#availability">Availability</a> | `availability`  | 
| 4 | Fixing regressions (things that worked before) | `regression` |
| 5 | <a href="/handbook/engineering/workflow/#infradev">Infradev</a> | `infradev`, `performance-refinement` |
| 6 | Promised to Customers | `planning-priority`, `customer`, `customer+` |
| 7 | Instrumentation improvements, particularly for xMAU | `instrumentation` |
| 8 | Usability Improvements and User Experience to drive xMAU |`feature::enhancement`, `UX debt`|
| 9 | IACV Drivers | |
| 10 | Identified for Dogfooding | `Dogfooding::Build in GitLab`, `Dogfooding::Rebuild in GitLab` |
| 11 | Velocity of new features, technical debt, community contributions, and all other improvements | `direction`, `feature`, `technical debt` |
| 12 | Behaviors that yield higher predictability (because this inevitably slows us down) | `predictability` |

*indicates forced prioritization items with SLAs/SLOs

### Engineering Allocation

Engineering is the DRI for mid/long term team efficiency, performance, security (incident response and anti-abuse capabilities), availability, and scalability. The expertise to proactive identify and iterate on these are squarely in the Engineering team. Whereas Product can support in performance issues as identified from customers. In some ways these efforts can be viewed as risk-mitigation or revenue protection. They also have the characteristic of being larger than one group at the stage level. Development would like to conduct an experiment to focus on initiatives that should help the organization scale appropriately in the long term.  We are treating these as a percent investment of time associated with a stage or category. The percent of investment time can be viewed as a prioritization budget outside normal Product/Development assignments.  

Unless it is listed in this table, Engineering Allocation for a stage/group is 0% and we are following normal [prioritization](handbook/product/product-processes/#prioritization).

This is engineering led and the EM is responsible for recognizing the problem, creating a satisfactory goal with clear success criteria, developing a plan, executing on a plan and reporting status.  It is recommended that the EM collaborate with PMs in all phases of this effort as we want PMs to feel ownership for these challenges.  This could include considering adding more/less allocation, setting the goals to be more aspirational, reviewing metrics/results, etc.   We welcome strong partnerships in this area because we are one team even when allocations are needed for long range activities.

| Group/Stage | Description of Goal | Justification | Maximum % of headcount budget | Supporting information | EMs | PMs | 
| ------ | ------ | ------- | ------ | ------ | ------- |  ------ | 
| Verify | Scale GitLab.com to 20M builds a day | Give us 2 years of runway | 10% | [CI Scaling Target](https://gitlab.com/groups/gitlab-org/-/epics/5745) | @darbyfrey | @jreporter |
| Verify:Runner | Improve Runner Deployments and ease adoption | Increase team efficiency/standardization |  30% | [CD for Runner](https://gitlab.com/groups/gitlab-org/-/epics/1453) | @erushton | @DarrenEastman | 
| Manage:Access | Improve performance of AuthorizedProjectsWorker | Following work to improving performance for Namespaces |  30% | [Improve performance of AuthorizedProjectsWorker](https://gitlab.com/groups/gitlab-org/-/epics/3343) | @lmcandrew | @mushakov |
| Gitaly | Improve performance on Authn/Authz with rewrite Rails->Go | Improve user experience at Git operation level |  15% | [Epic](https://gitlab.com/groups/gitlab-org/-/epics/5717) | @zj | @mjwood |
| Database | Primary Key overflow, Retention Strategy, Schema Validation, migration improvements, testing | Database has been under heavy operational load and needs improvement | 100% | [Automated Migration testing](https://gitlab.com/groups/gitlab-org/database-team/-/epics/6), [Automated migrations for primary key conversions](https://gitlab.com/groups/gitlab-org/-/epics/5654), [Remove PK overflow](https://gitlab.com/groups/gitlab-org/-/epics/4785), [Schema Validation](https://gitlab.com/groups/gitlab-org/-/epics/3928), [Reduce Total size of DB](https://gitlab.com/groups/gitlab-org/-/epics/4181) | @craig-gomes | TBD |

