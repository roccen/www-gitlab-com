---
layout: handbook-page-toc
title: "Jobs to be Done (JTBD) Overview"
description: "JTBD is a framework for viewing products and solutions in terms of jobs customers want to achieve. It's about understanding the goals that people want to accomplish."
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## JTBD Overview

A Job to be Done (JTBD) is a framework, or lens, for viewing products and solutions in terms of the jobs customers are trying to achieve. It's about understanding the goals that people want to accomplish. It lets us step back from our business and understand the objectives of the people we serve. It opens the door to innovation. 

At GitLab, we have our own flavor of JTBD and use it throughout the design process, but most notably to: 

* Define scope
* Validate direction
* Evaluate existing experiences
* Assess category maturity

JTBD come directly from research and customer conversations with those people who do the tasks/jobs we need to design for. [Problem validation](/handbook/engineering/ux/ux-research-training/problem-validation-and-methods/#what-is-problem-validation) is one of the most effective ways to confidently inform the writing of a JTBD. 

If you're looking for a deep dive into our JTBD philosophy, you can learn more about [what is and isn't a JTBD](/handbook/engineering/ux/jobs-to-be-done/deep-dive/#what-is-a-jtbd) and [JTBD core principles](/handbook/engineering/ux/jobs-to-be-done/core-jobs-to-be-done-principles/).

Watch the following video for a brief overview of why JTBD are so valuable towards the product development process:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/H6j1Xd4yufI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

For a quick how-to, follow the below guidance.

### JTBD terminology
First, let's set the terminology we use for JTBD.

* **Job**: something a customer wants to accomplish. For example, the main job of a GitLab customer could be stated as, "build and deploy software." The job always starts with a verb. 
* **Job Performer**: the person who does the job. Usually, we talk about these people in terms of personas. They are [buyers](/handbook/marketing/strategic-marketing/roles-personas/#buyer-personas), [developers](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer), [sysadmins](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator), [and so on](/handbook/marketing/strategic-marketing/roles-personas/#personas). 
* **Task**: a step in the process of completing a job. 
* **Need**: requirements for the job. These can be a system, business, or user requirement. Examples may include words like fast, inexpensive, efficient, less, more, must have, should have. 
* **Situation**: describes the circumstances a person is in when they need a job done.
* **Outcome**: the desired end state and/or feeling that a job performer has for doing a job. 
* **Job statement**: a succinct statement that brings together the need, situation, and outcome of a job. 

### How to write JTBD
We write our job statements using the standard format:

**"When [situation], I want to [job], so I can [outcome]."**

JTBD are difficult to get right. Before you begin, you have to have a clear understanding of what someone is wanting to accomplish, and that understanding should be validated with past research and customer conversations.

It is crucial to ensuring that the job statements are grounded in experience and not theory. We must have a high level of confidence in our job statements before we can put them into use.

#### Quick methods to increase confidence
* Reference previous research and industry standards.
* Conduct [generative problem validation research](/handbook/engineering/ux/ux-research-training/problem-validation-and-methods/#when-to-use-problem-validation) (broad questions).
  * For example, ask questions like, "tell me what you do as a software engineer." 
* Run abbreviated 30-minute job interviews with a minimum of 5 participants (direct questions).
  * For example, ask questions based on the JTBD such as, "tell me about the last time you created a multi-stage pipeline. What went well? What didn't go so well?"

### JTBD, user stories, and tasks
Job statements are different than user stories and tasks. They are designed to be persona, product, and solution agnostic and have a close relationship with user stories and tasks. This allows us to think more deeply about the context, rather than just a *role with a goal*.

*Example:*

**Job statement:**  

When my development ecosystem begins to mature, I want an overall understanding of my organization’s registries and specific package usage, so I can make effective architectural decisions.

**User story:**  

As an Engineering Leader, I want to know what packages are being used and by which projects, so that I can ensure my team is using the correct versions and not introducing risk into the pipeline.

**Task:**  

View a list of packages we use. 

In its most basic form, you will have a job statement that is associated with one or more user stories that are made up of multiple tasks. You'll use each of these at different moments within the design process.

Job statements offer a high-level view of the main objective. User stories guide your solutions as you create wireframes and tasks become the steps required to complete the job. 

If you want a detailed breakdown of each segement of the job statement, learn more about the [structure of a JTBD](/handbook/engineering/ux/jobs-to-be-done/deep-dive/#how-do-i-structure-a-jtbd). 

#### Determining scope of a JTBD
Job statements can be written at different levels or altitudes. For the majority of our work, we write job statements for stage groups as we craft experiences for features or sets of related features. 

JTBD can also be written for stage or cross-stage jobs to help determine longer-term product direction.

If you're writing a job statement for your stage group, consider this guiding principle to determine the appriopriate altitude: If the job is applicable to more than 3 user types, it's likely the altitude is set too high. 

### Prioritizing JTBD
Often we find there are many JTBD for one category. Using [user research](/handbook/engineering/ux/jobs-to-be-done/prioritizing-jobs-to-be-done) to help determine which JTBD are the most crucial to our users can help when planning for future research, design, and product needs.

