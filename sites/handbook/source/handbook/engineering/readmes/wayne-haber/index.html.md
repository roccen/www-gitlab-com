---
layout: markdown_page
title: "Wayne Haber's README"
job: "Engineering Director Threat Management"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Wayne Haber README

I’m the director of engineering for the [growth](https://about.gitlab.com/handbook/engineering/development/growth/) and [fulfillment](https://about.gitlab.com/handbook/engineering/development/fulfillment/) departments.  I am also a SME for security at the company.

Links about Wayne:
* [GitLab](https://gitlab.com/whaber)
* [LinkedIn](https://www.linkedin.com/in/waynehaber/)
* [Twitter](https://twitter.com/waynehaber)
* [WayneHaber.com](https://www.waynehaber.com)

I’m a veteran of three successful startups (including GitLab) and have experience in multiple areas including healthcare, finance, and security.

### How to collaborate with Wayne

* I subscribe to [servant-leadership](https://en.wikipedia.org/wiki/Servant_leadership) 
* Learning is very important to me.  I read approximately one book a week and aspire to take one online class (Udemy or LinkedIn learning) every month or so.
* I subscribe to the philosophy of “saying what you are going to do and doing what you say”.
* I am an advocate of remote-work due to it being highly effective (on many levels) for both the company and for the individual.
* At work, nothing makes me happier than when:
  * A user benefits from a change developed by my team
  * A prospect becomes a customer (or an existing customer expands their usaage of the product) because of a feature developed by my team
  * A team member learns something new and expands their horizons
  * A process or procedure or technology is improved that benefits the overall company, team members, or the technology industry as a whole
* I prefer to use this [1-1 format](/handbook/leadership/1-1/suggested-agenda-format/)
* I do skip-level meetings with those who work for my direct reports every two months.
* I do an AMA (ask me anything) / office hours session with my team approximately monthly.  Anyone at the company is invited to attend if they are so inclined.
* Some of my favorite business books include:
  * [How to Win Friends and Influence People](https://www.amazon.com/gp/product/B004U7G81O)
  * [The Five Dysfunctions of a Team](https://www.amazon.com/gp/product/B006960LQW)
  * [The Phoenix Project](https://www.amazon.com/gp/product/B0030V0PEW)
  * [Start with Why](https://www.amazon.com/gp/product/B002Q6XUE4)
* I am in Atlanta, GA, USA (Eastern US).
* I am most productive in the morning.
* When reaching out to me my preferences are (in order of preference):
  * Tag me in merge request
  * Tag me in issue
  * Tag me in the (public inside GitLab) #sd_threat_mgmt_and_growth_managers slack channel
  * Send me a 1:1 message in Slack 
  * Send me an email
* Since we are successful at being the most transparent company in the world, I prefer communications to be public whenever possible. However, this sometimes does not come naturally to me as my experience before GitLab was at a security company where confidentiality was of the utmost importance.
* I prefer that when possible, all recorded meetings should be made public on GitLab Unfiltered.  I am often pleasantly surprised by how this can increase collaboration both inside and outside the team.
* I always try to explain the “why”.  Engineers often leave that out and focus on the “what” and “how”.
