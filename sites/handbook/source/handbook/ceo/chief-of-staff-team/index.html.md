---
layout: handbook-page-toc
title: "Chief of Staff Team"
description: "GitLab CoS Team Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

----


## Team Mission
Compliment the CEO through being the cross-functional linchpin for GitLab when it comes to strategy and operations.

## Quick Links and Fun Facts
* [Chief of Staff Job Family](/job-families/chief-executive-officer/chief-of-staff/)
* [Strategy & Operations Job Family](/job-families/chief-executive-officer/strategy-and-operations/)
* [Performance Indicators](/handbook/ceo/chief-of-staff-team/performance-indicators/)
* CoS = Chief of Staff
* CoST = Chief of Staff Team
* [Project](https://gitlab.com/gitlab-com/chief-of-staff-team/cos-team)

## Contact Us
* [chief-of-staff-team](https://gitlab.slack.com/archives/CN7MPDZF0/p1568035351000200) on Slack

## Kinds of projects the CoS Team works on
{:#what-projects-does-the-cost-work-on}

The Chief of Staff and their team may work on projects that fit *any combination* of the following:
* projects that are many-functional
* projects that are important but not urgent or are under resourced
* projects that are so broad that it can't live within a function but are too much work for the CEO
* projects that are important to the CEO

This is not an exhaustive list of the types of work the CoS might do.

The CoST works closely with the [CEO](/job-families/chief-executive-officer/), the [E-Group](/company/team/structure/#e-group), the [EBA to the CEO](/job-families/people-ops/executive-business-administrator/), and [CEO Shadows](/handbook/ceo/shadow/).

#### Many Functional

GitLab is a [functional organization](/handbook/leadership/#no-matrix-organization), which means the [people are organized by function](/company/team/org-chart/).
Usually, when a project arises between two Departments, they can work something out on their own.
When a project arises between three or more Departments, the Chief of Staff will be the point person to execute.
In many cases, the Chief of Staff will be the [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/).
Whether it's a product feature proposal, a new CI job for job families, or questions from the board, the CoS is the person who can be trusted to get things done, get them done quickly, and get them done right.

Examples of a cross-functional project:
* Helping shepherd KPI and/or OKR progress
* Learning and development initiatives shared by the Sales Enablement teams and the People org
* Helping ensure job families have the required parts

#### Underresourced

As GitLab grows, projects will come up that are important but are under resourced.
Chiefs of Staff are known for their ability to become 80% effective on any subject quickly.
They are generalists at their core and, while they bring special skills to the table, they are meant to be able to address important problems as they come up.
A CoS might help source candidates for a strategic hire, fix grammatical errors in the handbook, and build a financial model all in the same day based on what is important or top of mind for the CEO at a given point.
The goal of the CoS is not to do the work of other teams, but help address work that those teams may not have bandwidth to address but are important to the organization and/or the CEO.

#### No clear leader

There may be projects with no clear leader for a myriad of reasons, including we're still hiring the point person or the lead is on leave.
Because of the CoS's ability to come up to speed quickly, they may be tasked something totally out of their domain with the expectation that they bring their leadership experience to the table, will do the work to make good decisions, and will lean on team members who are SMEs.

Examples of a project with no clear leader:
* Learning and development initiatives shared by the Sales Enablement teams and the People org
* [Internal Communications](/handbook/communication/internal-communications/)

#### Broad

Some projects or initiatives are very broad and cross-functional and *make sense* to belong to the CEO but are not a strategic use of the CEO's time.
OKRs are a prime example. OKRs need to happen and are key to the business but it is not efficient for the CEO to shepherd the process along.
The CoST is the shepherd for these sorts of projects and collaborates with all team members at GitLab to achieve success on such initiatives.

Examples of broad projects:
* E-group offsite prep
* Board meeting prep
* OKR shepherding

#### Important to the CEO
The CEO will have other projects that come up that he will task the CoST with, such as following up on something or carrying on a conversation on his behalf.

Examples of tasks that are important to the CEO:
* Handbook MRs
* Values updates
* Prepping for calls

## How to Work

The CoST works through a doc titled "Chief of Staff, Cheri, and Sid."
It's format is structured like the [1-1 Suggested Agenda Format](/handbook/leadership/1-1/suggested-agenda-format/).
Many of the tasks on the sheet are quick asks- handbook MRs, formatting changes, or questions to be answered.
Small asks should be handled as quickly as possible.
Other asks, such as OKR-related planning or an initiative that requires alignment with multiple stakeholders, requires forethought and more appropriate timing.
Some amount of time each week needs to be spent moving these sorts of tasks forward.

As a rule, everything in the doc is a TODO for the CoST.
When tasks are DONE, they should be labelled as such.
The CEO will review and delete the item once it's been assessed as completed.

We work from the bottom up in agendas, unless told to prioritize otherwise.

## Board Meetings

The Chief of Staff plays a key role in support Board Meetings.

The Board Meetings page is the single source of truth for information on the Board, but some of the responsibilities of the Chief of staff include, as per the [timeline](/handbook/board-meetings/#timeline):

1. reaching out before the meeting to collect questions from the board members.
1. sending reminders, preparing the agenda and chairing the meeting.
1. sends a reminder to the e-group two weeks in advance of the meeting.
1. distributing the board materials the Friday before the meeting.
1. ensuring that PDF versions of the materials including presentations, exhibits, minutes, option grants are stored in the Board of Directors folder on Google Drive in a folder labeled with the date of the meeting.
1. coordinating the [CEO Video](/handbook/board-meetings/#ceo-video-and-memo)
1. grouping questions in the [Agenda](/handbook/board-meetings/#agenda)

## OKRs

The CoS team has been running the [OKR process](/company/okrs/).
We set OKRs on a [fiscal quarter](/handbook/finance/#fiscal-year) basis.

There is an [OKR schedule](/company/okrs/#schedule) that dictates the timeline of events.
We use a handbook page for each quarter.
The CEO's Objectives every quarter map to the [sequence](/company/strategy/#sequence) of our [strategy](/company/strategy/#strategy).
The CEO's KRs are what we're measuring for the company for that quarter.

## KPIs

While OKRs are [what's changing](/company/okrs/#okrs-are-what-is-different) every quarter, what we're focused on moving (improving, being more efficient, etc.), [KPIs](/company/kpis/) are how we're consistently measuring how we're doing as an organization.
KPIs occur at multiple [layers](/company/kpis/#layers-of-kpis) and have [multiple parts](/company/kpis/#parts-of-a-kpi).
The CEO maintains [an index of GitLab KPIs and links to where they are defined](/company/kpis/).
There is a process for [updating the list](/handbook/business-ops/data-team/kpi-index/#updating-an-existing-kpi) by adding, removing, or changing a KPI.


## General Group Conversation

[Group Conversations](/handbook/group-conversations/) are updates on different parts of the company every 6 weeks.
The Chief of Staff prepares the General Group Conversation slides for the CEO.
During the General Group Conversations, please help facilitate the flow and ask team members to verbalize.
The CEO gives a General Group Conversation that covers the whole of the company.
The CEO's GC slides usually cover:
* OKR progress
* GitLab KPIs
* A reminder on handbook first
* Openings on the CEO Shadow Rotation
* Timely announcements
* Iteration Office Hours
* Three things that are on the CEO's mind, usually from the CoS agenda

The Group Conversations are stored in the "CEO Evangelism" folder on Google Drive.

### How to prepare the Slides

* Start by copying the last slide deck. This will form the skeleton.
* OKRs may need to be updated for the quarter, depending on the date.
* Update with timely announcements, e.g. E-Group Offsite updates, anything shared in Company FYI recently, any great wins.
* If there aren't any timely announcements, take this opportunity to remind folks about some of the key features of our [Communication](/handbook/communication/) guidelines, including our Top Misused Terms, MECEFU, or Why we track public Slack messages.
* Review the [recent CEO Shadow MRs](https://gitlab.com/dashboard/merge_requests?scope=all&utf8=✓&state=all&label_name[]=ceo-shadow) and the CoS agenda for anything that could be added to the "Top of Mind" section.
* Ping the two current and two former [CEO Shadows](https://about.gitlab.com/handbook/ceo/shadow/)in #ceo-shadow with the deck and request that they each add 1 slide based on their observations, topics that stood out or would be good to highlight at the company level from their shadow rotation.  

Be sure that slides are prepared with enough notice for the CEO to record a video and for it to be shared at least 24 hours in advance of the Group Conversation.

## E-Group Offsite

The executives get together every quarter for the [e-group offsite](/company/offsite/).
The CoS plays an important [role](/company/offsite/#roles).
It's [3-days long](/company/offsite/#schedule) with an All-Directs the following day.
[There is a book](/company/offsite/#book-choice).
There are [recurring content discussions](/company/offsite/#recurring-discussion-topics).
Here is feedback on the last [offsite all-directs meeting](https://gitlab.com/gitlab-com/cos-team/-/issues/18).

## CEO Performance Evaluation

In Q1 of a new fiscal year, the Chairperson of the Compensation Committee conducts the annual CEO Evaluation. The Chairperson meets with all members of GitLab's Board and E-Group for their feedback on the CEO's performance over the past Fiscal Year. The Chairperson meets with the CEO for their self-assessment at the beginning of the evaluation cycle. 

The CEO's self assessment is centered on three main areas
1. Areas of Notable Success 
1. Areas of Disappointment 
1. Areas for Improvement and Focus in the next fiscal year 

The Staff EBA to the CEO assists the Chairperson to schedule the performance review meetings with each Board Member (50 minutes, 1:1) and E-Group member (25 minutes, 1:1). These are conducted in an interview format to capture the richest possible feedback regarding the CEO's performance. During these calls, Board Members can expect to share their perspectives on the CEO's major accomplishments and disappointments in areas such as: vision, strategy, operations, management team development, company culture and relationship with the Board. General areas of strength. Areas for improvement and/or additional focus. Key fiscal year strategic/operational, non financial goals. 

The results from the Board and E-Group team interviews are summarized (without attribution) by the Chairperson and shared for discussion at the March Board of Directors meeting. 

The CoS is responsible for an update every other quarter to the Board on the progress made across focus areas. This will come in the form of a progress scorecard.
For example, if one area of focus is "set 3-year strategy", the CoS will evaluate whether the activity is on track or needs attention. The scorecard will be updated with a progress score (on track, needs attention, or at risk) and a high level summary of relevant key activities.

## Fiscal Year Kickoff

The [Fiscall Year Kickoff](/company/gitlab-assembly/) is the only all-hands-style meeting at GitLab.
The CoS is responsible for organizing it.

## Managing the All Directs

The [All-Directs group](/company/team/structure/#all-directs) is made up of anyone who reports directly to the e-group.
The CoS enables and manages this group.

## Maintaining the Biggest Risks and Tailwinds

We outline our [biggest risks](/handbook/leadership/biggest-risks/) and our [biggest tailwinds](/handbook/leadership/biggest-tailwinds/) in the handbook.
The CoS is responsible for maintaining this list.
There is an issue to also [add DRIs and review the mitigations](https://gitlab.com/gitlab-com/cos-team/-/issues/20).

## Dates to Keep Track of

This is not the SSOT for these dates and is meant only for informational/organizational purposes.

**Informal Board Meetings**
* Monthly on the 3rd Friday except in months where a formal BOD Meeting is scheduled 

**NomGov Meetings**
* 2021-03-15
* 2021-09-17

**Board Meetings**
* March
* June
* September
* December

**E-group Offsites**
* January
* April
* June
* October

## Daily Standup

On the CoST, we use [Geekbot](https://geekbot.com) for our Daily Standups.
These are posted in #chief-of-staff-team-standups in Slack.
Once team members are added to the daily standup list, they will receive a message from Geekbot via DM once they've been active on Slack after 6 AM in their local timezone.
There is no pressure to respond to Geekbot as soon as it messages you.
When Geekbot asks, "What will you do today?" try answering with specific details.
Give responses to Geekbot that truly communicate to your team what you're working on that day, so that your team can help you understand if some priority has shifted or there is additional context you may need.

## Chief of Staff Shadow

The Chief of Staff may occasionally have a Chief of Staff Shadow, a GitLab team member who will participate in a specific project or initiative for a fixed period of time. Shadow responsibilities could include: taking notes, providing feedback, and/or supporting the overall initiative success. This role would be in addition to any existing responsibilities at GitLab. Participants would opt in to experience another function within GitLab and contribute to a different part of the business. Since participation would be in addition to an existing workload, managers must sign off before a CoS Shadow can participate. Interested team members can share their interest with the Chief of Staff.

If you are interested in participating in the Chief of Staff Shadow program, open a merge request to add your name to the below table, assign to yourself and slack the `#chief-of-staff-team` for review and merge.

|  Start Date | End Date | First & Last Name | GitLab Handle |
| --- | --- | --- | --- |
|2020-05-23|2020-08-21  | Mike Miranda | @mmiranda |
|2020-08-31|2020-12-18  | Jerome Ng | @jeromezng |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |
|   |  |  |  |

### Important things to note
1. This is not a performance evaluation
1. Plan to observe and ask questions.
1. Participating in the shadow program is a privilege where you will be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present, and future honoring this trust placed in them.
1. Give feedback to and receive feedback from the Chief of Staff. Participants in the shadow program are encouraged to deliver [candid feedback](/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback). Shadows maintaining confidentiality during the program is separate from shadows being able to provide candid feedback.

## Resources on the CoS Role

* [The Unrepentant Generalist: How to Be a Great Chief of Staff in Tech](http://www.nehrlich.com/blog/2019/10/31/how-to-be-a-great-chief-of-staff-in-tech/)
* [NYT: Hail to the Chief of Staff](https://www.nytimes.com/2019/11/07/style/what-does-a-chief-of-staff-do.html)
* 2020 March: [Chief of Staff in the Tech Industry](https://medium.com/@alexismonville/chief-of-staff-in-the-tech-industry-c7dc3a43dae6)
* [Emilie Schario's Notes on the CoS Book](https://docs.google.com/document/d/1ZjWmqhv78eic57gxR825FvC9GMHo91pYakb3Jc7ximU/edit?usp=sharing)
* [Chief of Staff Tech Network](https://costechnetwork.com/)
* [Chief of Staff Resources](https://www.chiefofstaff.expert)
* [Prime Chief of Staff](https://primechiefofstaff.com)
* [The First 90 Days](https://medium.com/@robdickins/a-90-day-impact-plan-for-a-new-chief-of-staff-97768d9b04bd)
* [I’ve Logged 10,000 Hours as a Chief of Staff in a Large Tech Company](https://medium.com/@robdickins/ive-logged-10-000-hours-as-a-chief-of-staff-in-a-large-tech-company-here-s-my-pov-on-the-role-7c4aa095f5e8)
* [The Role of a Corporate Chief of Staff](https://medium.com/cos-tech-forum/part-1-the-role-of-a-corporate-chief-of-staff-8db0142318f1)
* [Better CoS: Decision Making](https://medium.com/@robdickins/better-cos-decision-making-5d97d14152e3)
* [Becoming a Chief of Staff with Brian Rumao](https://www.linkedin.com/learning/become-a-chief-of-staff-with-brian-rumao)
* [HBR: The Case for a Chief of Staff](https://hbr.org/2020/05/the-case-for-a-chief-of-staff)
