---
layout: handbook-page-toc
title: "Sales Finance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Finance Handbook!

## Common Links
 * [Financial Planning & Analysis (FP&A)](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/)
 * [Sales Strategy & Analytics (SS&A)](https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/)
 * [GTM Analytics Hub](https://about.gitlab.com/handbook/finance/financial-planning-and-analysis/Sales-Finance/GTM-Analytics-Hub)

### Finance Business Partner Alignment

| Name | Function |
| -------- | ---- |
| @fkurniadi | Overall |
| @alcurtis | Consolidation, Enterprise Sales |
| @bmenegocci | Commercial Sales, Channels, Alliances |
| @ysun3 | Customer Success, Professional Services, Field Ops |

## Sales Forecast Rhythm
We believe an excellent forecasting process enables us to deploy our resources effectively, risk-manage the business, and provide early warning systems. At GitLab, we design our Sales Forecast Rhythm to foster careful inspection and execution of bookings target throughout the quarter. Each week we review various aspects of the business, such as Current/Next Quarter pipeline, Renewals timing, and leading indicators KPIs, to name but a few.

[File](https://docs.google.com/spreadsheets/d/18LQD5B3E3EyV8abdVmDIyg1qL2qC2J9wwCOlBTuooIM/edit#gid=0)

<figure class="video_container">
<iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vStHiw-vsSJXuWFkB-iZ37wZXI1GXdI1bQpTin5TfU6v1_PWMFgSjxuttzgCqUeucagCiLPjJAmOKkq/pubhtml?widget=true&amp;headers=false"></iframe>
</figure>

## Sales Variance Package
[May'20 Deck](https://docs.google.com/presentation/d/10Dt_TdM8c6llZTEVkjbIxUCa5oA3Ghqbk76ec6rXK5A/edit?usp=sharing)

[Jun'20 Deck](https://docs.google.com/presentation/d/16Wrcgbzzpz2fs3MdoYtugI2KwM93M6aZ22UUYCWklEI/edit?usp=sharing)

[Jul'20 Deck](https://docs.google.com/presentation/d/1mDXoBdj8fOHfGLezTPJOPzgsi00raDDiuVJ3NkdUe38/edit?usp=sharing)

[Aug'20 Deck](https://docs.google.com/presentation/d/1aFfCku5LhSgIkurWgDTauvBqMNaaGi_L9e8JkXXxMxo/edit?usp=sharing)

[Sep'20 Deck](https://docs.google.com/presentation/d/1bevI8Qdu6bTLJ6arBUzKS8dOhK9d2SyfXAO9Z1n9fKk/edit?usp=sharing)

[Oct'20 Deck](https://docs.google.com/presentation/d/1mvIcU03htb4MhlvX-67UUSfAeR_gCEGJkUyqpSWcPDk/edit?usp=sharing)

[Nov'20 Deck](https://docs.google.com/presentation/d/1BU7JLYY_8I0qE-twteHHoa0XSmDuWFFDZ-YYyPCWVI0/edit?usp=sharing)

[Dec'20 Deck](https://docs.google.com/presentation/d/13sPhVbyDjZi67f3cTsq-1Z_YeDlkqeVBOnWPtf6vETo/edit?usp=sharing)

[Jan'21 Deck](https://docs.google.com/presentation/d/1UC1xFGZmw7bQJnYZFDjSGmchvqzFEihJTcbMf9FnYq8/edit?usp=sharing)

[Feb'21 Deck](https://docs.google.com/presentation/d/1f3AG8g_jfC-6eFurbwQQbZgvj2zTwUIDbwkXoG8gcHw/edit?usp=sharing)

## Sales & Marketing Funnel SSOT Dashboards
The following Single Source of Truth (SSOT) dashboards provide a monthly and quarterly Sales & Marketing funnel trend view, including; 
- IACV Pipeline Created
- Average days to SAO and to Closed Won
- $ & # of Stage 1+ opportunities
- $ & # of Closed Won opportunities
- ASP
- Win rate

Seperate dashboards have been created for each Stamped User Segment, and for only First Order deals. Each dashboard gives you the ability to filter for Web Direct deals, and SDR generated opportunities to enable the user to obtain multiple views of the data. 

[README](https://bit.ly/3aMJz6n)

[SFDC Dashboard - Leads](https://gitlab.my.salesforce.com/01Z4M000000oXUb)

[SFDC Dashboard - Opportunities - First Order - Pipe/SAO - Quarterly view](https://gitlab.my.salesforce.com/01Z4M000000oY9r)

[SFDC Dashboard - Opportunities - First Order - Closed Won - Quarterly view](https://gitlab.my.salesforce.com/01Z4M000000oXZW)

[SFDC Dashboard - Opportunities - First Order - Pipe/SAO - Monthly view](https://gitlab.my.salesforce.com/01Z4M000000oY9m)

[SFDC Dashboard - Opportunities - First Order - Closed Won - Monthly view](https://gitlab.my.salesforce.com/01Z4M000000oXUR)

[SFDC Dashboard - Opportunities - Large Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXZb)

[SFDC Dashboard - Opportunities - Large Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXZg)

[SFDC Dashboard - Opportunities - Mid-Market Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXd4)

[SFDC Dashboard - Opportunities - Mid-Market Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXcu)

[SFDC Dashboard - Opportunities - SMB Segment - Quarterly View](https://gitlab.my.salesforce.com/01Z4M000000oXdT)

[SFDC Dashboard - Opportunities - SMB Segment - Monthly View](https://gitlab.my.salesforce.com/01Z4M000000oXdd)


## Pipeline Velocity
This file details how much Net IACV has been closed relative to the quarterly target and the pace it has been booked at throughout the quarter.  The current quarter performance is 
compared to other historical quarters, to highlight whether the speed at which the net iACV has been closed is ahead or behind that of historical performance. 

In addition, the file also analyses how open pipeline is progressing for the next quarter relative to its quarterly target and the pace at which new opportunities are being added. This is 
also compared to historical quarters to inform whether the current quarter is ahead, in line, or behind the pace of previous quarters.

[File](https://docs.google.com/spreadsheets/d/1O81k_XpInMqn_pLPdbPJruh_nq0QQthJ9-nOiXZzBf0/edit#gid=2079563316)
[Dashboard] (https://app.periscopedata.com/app/gitlab/799969/WIP:-NF---Pipeline-X-Ray-Support-Graphs)

## Quota & Capacity Model
High-level long-range Quota & Capacity model to assess the feasibility of future bookings target and HC needs.

[File](https://docs.google.com/spreadsheets/d/1SzTLYRGYsNUWxijvwryi2i5O2Lh1FpUGwe8RPLi7WwQ/edit?usp=sharing)

## LAM Dashboard
This dashboard analyzes the Landed Addressable Market (LAM) offered by our existing customer base

[README](https://docs.google.com/presentation/d/1OfxlJb1F-dndEQZ2zozppGJm1J6CyxN79MfABeh5M-g/edit#slide=id.g8fb2a2c968_1_77)

[SFDC Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXOd)

## Net IACV Pipeline Movement
This file is intended to detail how the pipeline evolves throughout the quarter: capturing deals in the initial pipeline, those added later in the quarter, closed deals, slipped deals, and those remaining at quarter end.

[File](https://docs.google.com/spreadsheets/d/1L4Rl6hGb5t8x8f_3ILwUlYvOL7Rdo2Rh8FZf0RumBqE/edit#gid=1993507993)

## Professional Services (PS) Forecast Package
This file is intended to assess and forecast Professional Services department's performance.

[File](https://docs.google.com/spreadsheets/d/15YQV6dBpO06quxdvbZq6svTU-HgW06maZuhcwz7PYjI/edit#gid=1456058003)

## Bookings Dashboard
This dashboard provides a monthly trend view for Bookings, including dollar and count information. The 3 main sections of the dashboard are as follows:
- Bookings Trend: high-level view of the last 14 months for Net IACV, Renewal ACV, Won ACV, MYB, PS/PS Attach Rate, and TCV
- Net IACV Breakout: drill-down into the Net IACV components, such as FO, Growth, Expansion, Contraction, and Lost Renewals
- Next 6 Months Pipe: view into the pipeline for the next 6 months

[Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oXuF)

## Rolling List of Asks File
This file is intended to intake investment requests of both Headcount and Program Spend from Sales teams, prioritize them via a consistent framework, and implement them based on agreed-upon prioritization. For the Sales Headcount Change Management process, please refer to the [Sales Headcount](https://about.gitlab.com/handbook/sales/field-operations/sales-strategy/sales-headcount/) Handbook page located in Sales Strategy & Analytics.

[File](https://docs.google.com/spreadsheets/d/1NgnRnCQkDXRuLykroLLeNmWqKhIE1g0OaueSbFSUz6Q/edit#gid=0)

