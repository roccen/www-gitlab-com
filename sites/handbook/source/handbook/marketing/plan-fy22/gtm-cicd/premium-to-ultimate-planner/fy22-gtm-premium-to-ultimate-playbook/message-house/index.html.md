---
layout: handbook-page-toc
title: "Upsell Premium to Ultimate message house"
description: "description to add"
twitter_image: '/images/tweets/handbook-marketing.png'
---

### Message house

### Context

Budget and influence are key for this play, so our basic assumption is that upleveling to someone’s boss isn’t going to be enough *all the time* - people need to reach out to additional business units, namely security, and bring in “fresh” teams and opportunities within existing accounts. 

- Additionally, we need to enable and equip current Premium customers and their teams to be more “active champions” by showing them the light that is the value of Ultimate. When thinking about the value of Ultimate at the highest level 

Think *security and reducing risk, protecting the business's intellectual property and sensitive data, enabling modern CD with compliance and audit controls, and providing visibility/insights at different levels from ICs to executive insights*

**The who**

- Economic buyers: CISO or Security Manager, VP of Security, Director of Security, VP of IT or CTO, App/Dev Director

- Technical influencers: Chief Architect, App Dev Manager

### Messaging and Positioning

| **Positioning statement** | GitLab Ultimate helps you reduce and manage risk, protect your IP, and streamline the software delivery process by embedding security and compliance into your DevOps workflows, unifying teams under one roof. <br> <br> 
|-----------|-------------------------------------------------------------------------|
| **Elevator pitch** |  GitLab Ultimate is ideal for projects with executive visibility and strategic organizational usage. Ultimate enables IT transformation by optimizing and accelerating delivery while managing priorities, security, risk, and compliance. |
| **Long description** | GitLab Ultimate helps organizations and teams achieve advanced DevOps maturity via enterprise-level application security capabilities, advanced insights/analytics at the project, group, and instance level, and enterprise-grade priority support. This includes 24/7 uptime support with live upgrade assistance and named Technical Account Manager to assure you meet your goals **and** have expertise at your side to overcome challenges when you need it most. |

| **Key-Values** | Single app with end-to-end visibility and insights | Embedded security that’s contextual + congruent to DevOps processes | Leading SCM, CI, and code review in one | Modern CD with compliance |
|--------------|----------------------------------------------------------|--------------|--------------|
| **Promise** | wip |  |  |  |
| **Pain points** |  |  |  |  |
| **Why GitLab** |  |  |  |   |

Additional Ultimate value: 
- Enterprise-grade priority support, including 24/7 uptime support, a named Technical Account Manager (TAM), and live upgrade assistance are all still included with Ultimate.
- Access to free guest users
- Project, portfolio and requirements management [value](https://about.gitlab.com/pricing/ultimate/#project-portfolio-and-requirements-management)

| **Proof points** |  [Glympse](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/issues/22), [BI Worldwide](https://11211-wip-web-page-for-premium-to-ultimate-sales-play.about.gitlab-review.app/customers/bi_worldwide/), [Jasper](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/issues/49), [Wag!](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/issues/14), [HERE Tech](https://developer.here.com/blog/shifting-security-left-in-the-here-platform) |
