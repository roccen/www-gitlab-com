---
layout: handbook-page-toc
title: Learning and Development for Product Management
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**Principles**](/handbook/product/product-principles/) - [**Processes**](/handbook/product/product-processes/) - [**Categorization**](/handbook/product/categories/) - [**GitLab the Product**](/handbook/product/gitlab-the-product) - [**Being a PM**](/handbook/product/product-manager-role) - [**Performance Indicators**](/handbook/product/performance-indicators/) - [**Leadership**](/handbook/product/product-leadership/)

Welcome to Learning and Development for Product Management at GitLab! The resources here are meant to support product managers explore, learn and grow at their own pace. We aim to collect content that spans various skill levels, as well as various levels of depth/commitment. It is recommended that product managers engage with the resources here to help them have a successful journey at GitLab and in their product career as a whole. 

Most of the resources here are free but any content requiring payment [can be reimbursed following the GitLab reimbursement policies](https://about.gitlab.com/handbook/finance/expenses/#work-related-online-courses-and-professional-development-certifications).

Over time, we will add content to directly support GitLab's product management [competencies](/handbook/product/product-manager-role/#competencies), [CDF](/handbook/product/product-manager-role/#product-management-career-development-framework) and [product development flow](/handbook/product-development-flow/). We understand that the evolving product management space requires continuous learning, and GitLab is committed to providing the time needed for in-depth learning too as part of your working time. You are encouraged to ask your manager to help you carve out time for Learning and Development. 

### Recommended books and talks for all GitLab product managers

These books are highly recommended to be read by every product manager at GitLab:

- **[Marty Cagan: Inspired: How to Create Tech Products Customers Love](https://www.amazon.com/INSPIRED-Create-Tech-Products-Customers-ebook/dp/B077NRB36N)** <br>
Provides a general overview of Product Management <br>

<iframe src="https://player.vimeo.com/video/134431731" width="560" height="315" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>

- **[Melissa Perry: Escaping the Build Trap](https://www.amazon.com/Escaping-Build-Trap-Effective-Management/dp/149197379X/)** <br>
Describes how to build and operate a successful product team <br>

<iframe src="https://player.vimeo.com/video/224453335" width="560" height="315" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe> 

- **[Eric Ries: The Lean Startup](https://www.amazon.com/Lean-Startup-Entrepreneurs-Continuous-Innovation/dp/0307887898)** <br>
Discusses how to leverage efficiency to achieve optimal outcomes. <br> 

<iframe width="560" height="315" src="https://www.youtube.com/embed/RSaIOCHbuYw" title="YouTube video player" frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>

### Supporting materials for GitLab specific [product manager competencies](/handbook/product/product-manager-role/#competencies)

#### Opportunity Canvases 

GitLab's Chief Product Officer shares insights on what makes an opportunity canvas successful in a 10 minute video:


<iframe width="560" height="315" src="https://www.youtube.com/embed/f8k1jWejojA" title="YouTube video player" frameborder="0" allow="autoplay; encrypted-media;" allowfullscreen></iframe>

- This [opportunity canvas team chat (45 min video)](https://youtu.be/ZXbwtLoxPjM) is an example of how to approach identifying opportunities in preparation for doing a canvas.

## General Product Management learning content 

This content is divided into five key competencies for Product Managers. 

<!--**💡 [Discovery](#-discovery) &nbsp; &nbsp; &nbsp; ✏️ [Design](#%EF%B8%8F-design)  &nbsp; &nbsp; &nbsp;  🚀 [Delivery](#-delivery)  &nbsp; &nbsp; &nbsp;  📈 [Business Acumen](#-business-acumen)  &nbsp; &nbsp; &nbsp; 💬 [Communication](#-communication)  &nbsp; &nbsp; &nbsp;   🤝 [Team Management](#-team-management)**-->

## 💡 Discovery
<hr>
### Discovery: User research

##### Quick reads and videos

- [Paul Adams: Great PMs don't spend their time on solutions](https://www.intercom.com/blog/great-product-managers-dont-spend-time-on-solutions/)
- [Adopting Continuous Discovery Practices](https://youtu.be/a2HidFrzYIA) (36 min video)

##### Deeper dive

- [Introduction to Modern Product Discovery](https://medium.com/productized-blog/an-introduction-to-modern-product-discovery-by-teresa-torres-productizedconf-bb2703b01fdb) 
- [Just Enough Research](https://vimeo.com/129039134) (59 min video)

##### Online courses 

- [Nielson Norman Group Trainings and Events](https://www.nngroup.com/training/)


##### Books

- [Steve Krug: Rocket Surgery Made Easy: The Do-It-Yourself Guide to Finding and Fixing Usability Problems](https://www.amazon.com/Rocket-Surgery-Made-Easy-Yourself/dp/0321657292)
- [Kevin Simler & Robin Hanson: The Elephant in the Brain: Hidden Motives in Everyday Life](https://www.amazon.com/Elephant-Brain-Hidden-Motives-Everyday/dp/0190495995)
- [Daniel Kahneman: Thinking, Fast and Slow](https://www.amazon.com/Thinking-Fast-Slow-Daniel-Kahneman/dp/0374533555)
- [Erika Hall: Just Enough Research](https://abookapart.com/products/just-enough-research)

### Discovery: Customer interviewing

##### Quick reads and videos

- ['Get in the Van' and Other Tips for Getting Meaningful Customer Feedback](https://review.firstround.com/the-power-of-interviewing-customers-the-right-way-from-twitters-ex-vp-product)
- [The Ultimate List of Customer Development Questions](https://mfishbein.com/the-ultimate-list-of-customer-development-questions/)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

- [Continuous Interviewing by Product Talk](https://learn.producttalk.org/p/continuous-interviewing)

##### Books

- [Interviewing for Research by Andrew Travers](https://trvrs.co/book/)
- [Talking to Humans](https://www.amazon.co.uk/Talking-Humans-Success-understanding-customers-ebook/dp/B00NSUEUL4) ([alternate free pdf](https://s3.amazonaws.com/TalkingtoHumans/Talking+to+Humans.pdf))
- [The Mom Test: How to talk to customers & learn if your business is a good idea when everyone is lying to you](https://www.amazon.com/Mom-Test-customers-business-everyone/dp/1492180742)
- [Interviewing Users: How to Uncover Compelling Insights](https://www.amazon.com/Interviewing-Users-Uncover-Compelling-Insights/dp/193382011X)

### Discovery: Jobs to be done 

##### Quick reads and videos

- [Gitlab Jobs to be Done (JTBD) Overview](https://about.gitlab.com/handbook/engineering/ux/jobs-to-be-done/)
- [Clayton Christensen - Understanding the Job](https://www.youtube.com/watch?v=sfGtw2C95Ms) (5 min video)
- [Know Your Customers’ “Jobs to Be Done”](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done)


##### Deeper dive

- [GitLab Jobs to be Done (JTBD) Deep Dive](https://about.gitlab.com/handbook/engineering/ux/jobs-to-be-done/deep-dive/)
- [Bob Moesta & Chris Spiek - Uncovering the Jobs to be Done](https://businessofsoftware.org/2014/06/bos-2013-bob-moesta-and-chris-spiek-uncovering-the-jobs-to-be-done/) (57 min video)
- [Tony Ulwick - Customer Centered Innovation](https://businessofsoftware.org/2015/08/tony-ulwick/) (57 min video)
- [Xavier Russo - A step-by-step guide to using Outcome Driven Innovataion](https://medium.com/envato/a-step-by-step-guide-to-using-outcome-driven-innovation-odi-for-a-new-product-ded320f49acb)

##### Online courses 

- [Mastering Jobs-To-Be-Done Interviews](https://learn.jobstobedone.org/courses/JTBDinterviews)

##### Books

- [Clayton M. Christensen et al: Competing Against Luck](https://www.amazon.com/Competing-Against-Luck-Innovation-Customer-ebook/dp/B01BBPZIHM/)
- [Intercom on Jobs-to-be-Done](https://www.intercom.com/resources/books/intercom-jobs-to-be-done) (free ebook download)

### Discovery: Lean product development

##### Quick reads and videos

- [How to Build a Minimum Loveable Product](https://medium.com/the-happy-startup-school/beyond-mvp-10-steps-to-make-your-product-minimum-loveable-51800164ae0c#.56uv0dydd)


##### Deeper dive

- [Melissa Perri - Lean Product Management](https://vimeo.com/122742946) (42 min video)
- [How to Run Amazing Remote Design Sprints!](https://www.youtube.com/watch?v=IFHfsRNTGCM) (60 min video)
- [Design Sprint Exercise Video Series](https://www.youtube.com/playlist?list=PLxk9zj3EDi0X5CgoFckoheIFAx-uT2i7j)

##### Online courses 

_Please contribute your favorite resources here_

##### Books 
- [Eric Ries: The Lean Startup](https://www.amazon.com/Lean-Startup-Entrepreneurs-Continuous-Innovation/dp/0307887898)
- [Jake Knapp: Sprint: How to Solve Big Problems and Test New Ideas in Just Five Days](https://www.amazon.com/Sprint-Solve-Problems-Test-Ideas/dp/1442397683)
- [Ash Maurya: Running Lean: Iterate from Plan A to a Plan That Works](https://www.amazon.com/Running-Lean-Iterate-Plan-Works/dp/1449305172)

### Discovery: Growth and experimentation 

##### Quick reads and videos

- [Product Managers: It's Time to Move from Whole Product to Product-led Growth](https://expand.openviewpartners.com/product-managers-its-time-to-move-from-whole-product-to-product-based-go-to-market-304467093357#.1euz7drsb)
- [10 Statistics Traps in A/B Testing: The Ultimate Guide for Optimizers](https://cxl.com/blog/testing-statistics-mistakes/)
- [5 Common Threats to Your A/B Test’s Validity](https://instapage.com/blog/validating-ab-tests)

##### Deeper dive

- [Product Led Growth Resources](https://openviewpartners.com/product-led-growth-resources/)
- [GrowthHackers: Growth University](https://university.growthhackers.com/)

##### Online courses 

- [Reforge](https://www.reforge.com/all-programs)

##### Books

- [Sean Ellis: Hacking Growth: How Today's Fastest-Growing Companies Drive Breakout Success](https://www.amazon.com/Hacking-Growth-Fastest-Growing-Companies-Breakout/dp/045149721X)
- [Nir Eyal: Hooked: How to Build Habit-Forming Products](https://www.amazon.com/Hooked-How-Build-Habit-Forming-Products/dp/1591847788)
- [Trusworthy Online Controlled Experiments](https://www.amazon.com/Trustworthy-Online-Controlled-Experiments-Practical/dp/1108724264)
- [Intercom: The Growth Handbook](https://www.intercom.com/resources/books/growth-handbook) (free ebook download)



<br>
<br>
## ✏️ Design 
<hr>
##### Quick reads and videos

- [Julie Zhuo: How to Work with Designers](https://medium.com/the-year-of-the-looking-glass/how-to-work-with-designers-6c975dede146#.q68swu2de)
- [Jess Eddy: What do designers really want from product managers?](https://uxdesign.cc/what-do-designers-really-want-from-product-managers-9c0e14993a8)
- [Jared Spool: Using the Kano Model to Build Delightful UX](https://youtu.be/ewpz2gR_oJQ) (45 min video)

##### Deeper dive

- [Leaders of Awesomeness](https://leaders.centercentre.com/) 
- [The Complete Guide to the Kano Model](https://foldingburritos.com/kano-model/)

##### Online courses 

- [Accessibility for Web Design](https://www.linkedin.com/learning/accessibility-for-web-design/welcome?u=2255073)

##### Books

- [Don Norman: The Design of Everyday Things](https://www.amazon.com/Design-Everyday-Things-Revised-Expanded/dp/0465050654)
- [Steve Krug: Don't Make Me Think](https://www.amazon.com/Dont-Make-Think-Revisited-Usability/dp/0321965515)
- [Sketching User Experiences: The Workbook](https://www.amazon.com/Sketching-User-Experiences-Saul-Greenberg/dp/0123819598)
- [The User Experience Team of One: A Research and Design Survival Guide](https://www.amazon.com/User-Experience-Team-One-Research/dp/1933820187)


<br>
<br>
## 🚀 Delivery
<hr>
### Delivery: User stories

##### Quick reads and videos

- [How to split a user story](https://agileforall.com/how-to-split-a-user-story/)
- [Replacing The User Story With The Job Story](https://jtbd.info/replacing-the-user-story-with-the-job-story-af7cdee10c27)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

_Please contribute your favorite resources here_

##### Books

- [Jeff Patton: User Story Mapping: Discover the Whole Story, Build the Right Product](https://www.amazon.com/User-Story-Mapping-Discover-Product/dp/1491904909)

### Delivery: Backlog management

##### Quick reads and videos

- [Brandon Chu: Ruthless Prioritization](https://blackboxofpm.com/ruthless-prioritization-e4256e3520a9)
- [Sean McBride: RICE: Simple prioritization for product managers](https://www.intercom.com/blog/rice-simple-prioritization-for-product-managers/)
- [Ash Maurya: Love the Problem, Not Your Solution](https://blog.leanstack.com/love-the-problem-not-your-solution/)
- [Launch mode vs iterate mode](https://www.intercom.com/blog/launch-mode-vs-iterate-mode/)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

_Please contribute your favorite resources here_

##### Books 

- [Donald G. Reinertsen: The Principles of Product Development Flow](https://www.amazon.com/Principles-Product-Development-Flow-Generation/dp/1935401009)

### Delivery: Working with Engineering 

##### Quick reads and videos

_Please contribute your favorite resources here_

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

_Please contribute your favorite resources here_

##### Books

_Please contribute your favorite resources here_
<br>
<br>
## 📈 Business acumen
<hr>
### Business acumen: product strategy

##### Quick reads and videos

- [Marty Cagan: Vision Vs. Strategy](https://svpg.com/vision-vs-strategy/)
- [Des Traynor: Product strategy means saying no](https://www.intercom.com/blog/product-strategy-means-saying-no/)
- [Ken Norton's Discipline of No](https://mixpanel.com/blog/learning-to-say-no-with-ken-norton/)

##### Deeper dive

- [Des Traynor: Product Strategy Revisited](https://businessofsoftware.org/2014/12/product-strategy-saying-part-2-des-traynor-bos-usa-2014/) (54 min video)

##### Online courses 

Please contribute your favorite resources here

##### Books

- [Crossing the Chasm, 3rd Edition: Marketing and Selling Disruptive Products to Mainstream Customers](https://www.amazon.com/Crossing-Chasm-3rd-Disruptive-Mainstream-dp-0062292986/dp/0062292986)
- [Testing Business Ideas: A Field Guide for Rapid Experimentation](https://www.amazon.com/Rapid-Testing-Business-Ideas-Customer/dp/1119551447)

### Business acumen: competitive analysis

##### Quick reads and videos

- [Stop Ignoring Your Competitors...](https://producthabits.com/stop-ignoring-competitors/)
- [The Startup Chat: Episode 319: How To Do Competitor Analysis](https://thestartupchat.com/ep319/)

##### Deeper dive

- [SaaSFest Keynote: Building a Saas Company Isn't What it Used to Be](https://www.priceintelligently.com/blog/saasfest-keynote-speaker-hiten-shah-building-a-saas-business)

##### Online courses 

_Please contribute your favorite resources here_

##### Books

_Please contribute your favorite resources here_

### Business acumen: KPIs and Metrics 

##### Quick reads and videos

- [John Doerr: Why the secret to success is setting the right goals](https://www.youtube.com/watch?v=L4N1q4RNi9I) (5 min video)
- [David Skok: SaaS Metrics 2.0 – A Guide to Measuring and Improving what Matters](https://www.forentrepreneurs.com/saas-metrics-2/)

##### Deeper dive

- [Financial Modeling for Product Managers](https://tpgblog.com/2015/05/04/product-financial-modeling/) (60 min video)

##### Online courses 

- [LinkedIn Learning: SQL Essential Training](https://www.linkedin.com/learning/sql-essential-training-3/)

##### Books

- [Christina Wodtke: Radical Focus: Achieving Your Most Important Goals with Objectives and Key Results ( OKRs )](https://www.amazon.co.uk/Radical-Focus-Achieving-Important-Objectives-ebook/dp/B01BFKJA0Y)
- [Douglas W. Hubbard: How to Measure Anything: Finding the Value of Intagibles in Business](https://www.amazon.com/How-Measure-Anything-Intangibles-Business-ebook/dp/B00INUYS2U)
- [Measuring the User Experience: Collecting, Analyzing, and Presenting Usability Metrics](https://www.amazon.com/Measuring-User-Experience-Interactive-Technologies/dp/0124157815)

<br>
<br>
## 💬 Communication
<hr>
### Communication: relationships with customers

##### Quick reads and videos

- [How can I say this? Podcast](https://open.spotify.com/show/55AaTIqnERpvxwTVMxToC5)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

- [LinkedIn Learning: Advanced Business Development Communication & Negotiation](https://www.linkedin.com/learning/advanced-business-development-communication-and-negotiation/taking-it-to-the-next-level)

##### Books

- [Building a Story brand](https://www.amazon.com/Building-StoryBrand-Clarify-Message-Customers/dp/0718033329/ref=sr_1_11?)

### Communication: writing to inspire, align and activate 

##### Quick reads and videos

[Paul Graham: Write Simply](http://paulgraham.com/simply.html)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

_Please contribute your favorite resources here_

##### Books

_Please contribute your favorite resources here_

### Communication: presentations, prepared and adhoc

##### Quick reads and videos

_Please contribute your favorite resources here_

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

- [LinkedIn Learning: Communicating for Product Managers](https://www.linkedin.com/learning/communication-for-product-managers/your-role-as-diplomat-of-the-product-team?u=2255073)

##### Books

_Please contribute your favorite resources here_

##### Examples

- [Example CAB Presentation](https://docs.google.com/presentation/d/11YI6zj3qMeTwXTmdovuZLEM2i3D-eNMdNXcQgc9coc8/edit#slide=id.g85b09e34a4_0_5)

<br>
<br>
## 🤝 Team management 
<hr>
### Team management: stakeholder management

##### Quick reads and videos

_Please contribute your favorite resources here_

##### Deeper dive

- [Rosemary King: Stakeholders, let 'em in](https://www.mindtheproduct.com/stakeholders-building-an-open-door-culture/) (20 min video)

##### Online courses 

- [Managing Project Stakeholders](https://www.linkedin.com/learning/managing-project-stakeholders-2?u=2255073) (LinkedIn Learning course)

##### Books

_Please contribute your favorite resources here_

### Team management: cross-functional team management

##### Quick reads and videos

- [HBR: What cross functional teams need to succeed](https://hbr.org/tip/2015/10/what-cross-functional-teams-need-to-succeed)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

- [LinkedIn Learning: Cross Functional Team Management](https://www.linkedin.com/learning/managing-a-cross-functional-team/welcome)

##### Books

- [Dare to Lead](https://www.amazon.com/Dare-Lead-Brave-Conversations-Hearts/dp/0399592520)
- [Radical Candor](https://www.amazon.com/dp/1529038340/ref=cm_sw_em_r_mt_dp_JW4418H1Q2BQ2MGV62WN)

### Team management: direct team management

##### Quick reads and videos

- [The New Manager Death Spiral](https://randsinrepose.com/archives/the-new-manager-death-spiral/)
- [How to Improve Your Emotional Intelligence to Become a Better Leader](https://getlighthouse.com/blog/how-to-improve-emotional-intelligence/)

##### Deeper dive

- [The New Manager Death Spiral](https://www.youtube.com/watch?v=pAbU3WJ-NBw) (30 min video)

##### Online courses 

- [LinkedIn Learning: Building High Performing Teams](https://www.linkedin.com/learning/building-high-performance-teams/building-a-high-performing-team)

##### Books

- [Kim Scott: Radical Candor](https://www.amazon.com/Radical-Candor-Kim-Scott/dp/B01KTIEFEE)
- [Michael Lopp: Managing Humans](https://www.amazon.com/Managing-Humans-Humorous-Software-Engineering-ebook/dp/B01J53IE1O/)
- [Julie Zhuo: The Making of a Manager: What to Do When Everyone Looks to You](https://www.amazon.com/Making-Manager-What-Everyone-Looks-ebook/dp/B079WNPRL2/)
- [First Round Essentials: Management](https://books.firstround.com/management/) (free ebook download)

### Team management: leadership and influence

##### Quick reads and videos

- [Start with why -- how great leaders inspire action](https://www.youtube.com/watch?v=u4ZoJKF_VuA) (18 min video)
- [Leadership Biz Cafe Podcast](https://www.tanveernaseer.com/lbc/)

##### Deeper dive

_Please contribute your favorite resources here_

##### Online courses 

- [LinkedIn Learning Path: Strategic Leadership](https://www.linkedin.com/learning/paths/develop-your-strategic-planning-skills)

##### Books

- [Start with Why: How Great Leaders Inspire Everyone to Take Action](https://www.amazon.com/Start-Why-Leaders-Inspire-Everyone/dp/1591846447)
- [Trillion Dollar Coach: The Leadership Playbook of Silicon Valley's Bill Campbell](https://www.amazon.com/Trillion-Dollar-Coach-Leadership-Playbook/dp/0062839268)
- [Extreme Ownership: How U.S. Navy SEALs Lead and Win](https://www.amazon.com/Extreme-Ownership-U-S-Navy-SEALs/dp/1250067057)

<br>
<br>
## Product management thought leadership

### Blogs, videos and podcasts and more...

There is a lot of amazing content and ongoing trends in the world of product development. Subscribing to blogs, video channels and other ongoing content streams is a great way to get inspiration on best practices and product innvoation with your team. Here are some recommendations on where to start:

#### Blogs

- [https://svpg.com/articles/](https://svpg.com/articles/)
- [https://www.intercom.com/blog/product-and-design/](https://www.intercom.com/blog/product-and-design/)
- [https://producthabits.com/blog/](https://producthabits.com/blog/)
- [https://www.mindtheproduct.com/](https://www.mindtheproduct.com/)
- [https://cutlefish.substack.com/](https://cutlefish.substack.com/)

#### Podcasts

- [Mind the Product](https://www.mindtheproduct.com/the-product-experience/)
- [Masters of Scale](https://open.spotify.com/show/1bJRgaFZHuzifad4IAApFR)  
- [Dollars to Donuts](https://open.spotify.com/show/26yTpSSgqzR58wdlFTEvis?si=FU8B_7sxQluT78PepGecGA&nd=1)  
- [Presentable](https://open.spotify.com/show/2HMGa6BesR174n8NP28VEu?si=5tSm84pmTvKhOD4313Ir8Q&nd=1)  

#### Newsletters

- [Mind the Product Newsletter](https://www.mindtheproduct.com/product-management-newsletter/)

#### Other

- [Mind the Product Slack channel](https://www.mindtheproduct.com/product-management-slack-community/)
- [GitLab team member Viktor Nagy on Twitter](https://twitter.com/nagyviktor)
- [Product League](https://productleague.com/)
- [Product School](https://www.youtube.com/channel/UC6hlQ0x6kPbAGjYkoz53cvA/featured) (YouTube Channel)
- [Women In Product](https://www.youtube.com/channel/UC6z_ibVMdKbKFfgb3mxwx9g/videos?view=0&sort=p) (YouTube Channel)


### Thought leaders and influencers

One of the best ways to stay in the know is to follow people! There are a lot of folks openly sharing their ideas and best practices. We encourage you to follow and exchange ideas with people who inspire you. Here are some recommendations on where to start:

- [Jackie Bavaro](https://twitter.com/jackiebo)
- [Marty Cagan](https://twitter.com/cagan)
- [John Cutler](https://twitter.com/johncutlefish)
- [Ken Norton](https://twitter.com/kennethn)
- [Jeff Patton](https://twitter.com/jeffpatton)
- [Roman Pichler](https://twitter.com/johncutlefish)



