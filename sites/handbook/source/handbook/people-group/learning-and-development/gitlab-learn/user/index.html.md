---
layout: handbook-page-toc
title: GitLab Learn User Documentation
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## GitLab Learn Users

The following resources can be used by learners in the [GitLab Learn](/handbook/people-group/learning-and-development/gitlab-learn/) platform to help navigate the login process, explore new content, and more.

**Have additional questions?**

GitLab team members can reach out to the Learning and Development team via the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W) for additional support with GitLab Learn.

#### A note about naming

Review the following terms to get familiar with language used in this documentation.

| Term | Definition |
| ----- | ----- |
| EdCast | The vendor we're collaborating with to create GitLab Learn. |
| GitLab Learn | GitLab's EdCast instance |
| Learning Experience Platform (LXP) | The type of learning platform that GitLab is using to organize learning content. Learn more in the [L&D handbook](/handbook/people-group/learning-and-development/#gitlab-learn-edcast-learning-experience-platform-lxp) |

## Support options for GitLab Learn users

If you're looking for support in getting your questions answered, please refer to the following resources:

| Audience | Support |
| ----- | ----- |
| GitLab team members | Please reach out to the L&D team in Slack for support via the [#learninganddevelopment channel](https://app.slack.com/client/T02592416/CMRAWQ97W/thread/C6H8647PS-1611605514.007500) |
| Customers | Please [submit a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000647759) for help from our Support or Professional Services team |
| Community Members | Please post your question in the in the [GitLab Community Forum](https://forum.gitlab.com/) or [submit a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000647759). |


### First time login to GitLab Learn

When you log on to GitLab Learn for the first time, you'll be prompted through the account setup process. The platform will use your answers to suggest new learning content based on your interests and expertise.


Below is a [firt time login training video](https://www.youtube.com/watch?v=uE-1KRFArpA&feature=youtu.be) to walk you through each step.

<iframe width="560" height="315" src="https://www.youtube.com/embed/uE-1KRFArpA" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

GitLab Learn first time login steps:

1. GitLab team members should choose `LogIn with GitLab Team Members SSO` via Okta. Non-team members should choose `LogIn with Email and Password`.
1. Enter your first and last name
1. Upload a profile photo. This step is optional at this phase of the sign on process and can be uploaded/edited within the platform at a later date
1. Click `Next Step`
1. Add up to 3 learning goals to your profile. Use the search bar to locate learning goals in the platform. These learning goals can be about any topic you are interested in learning about. Some examples could be `product marketing`, `technical writing`, or `leadership`. If you need help determining your learning goals, reach out to the [L&D team in slack](https://app.slack.com/client/T02592416/CMRAWQ97W)
1. When adding learning goals, you may get an error that reads `The goal you are trying to add does not exist`. This indicates that the goal you've entered does not yet exist in the database. Consider using a different learning goal or phrase. 
1. Click `Next Step`
1. Add skills to your profile. These skills should reflect your own expertise
1. Click `Next Step`
1. At this point, you will be dropped into the GitLab Learn platform on the Discover page, and can begin to explore GitLab learning content! 


Please note that at this time, the only option for authentication to GitLab Learn by users who are not GitLab team members will be using an email and password .


### Getting familiar with the platform

Watch this [video tour](https://youtu.be/hlChYGZcaNc) of the GitLab Learn platform to get familiar with what you'll see upon logging in.

<iframe width="560" height="315" src="https://www.youtube.com/embed/hlChYGZcaNc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

#### Navigating the discover page

When you first log into GitLab Learn, you'll arrive on the Discover page. This page will:

- Introduce you to trending topics on the platform and among other GitLab learners
- Provide quick access to your learning paths and assigned content
- Explore content channels to discover content you might be interested in
- Highlight initiatives from the GitLab L&D team and other content creators


### Using the top navigation bar

There are many resources you can access using this top navigation from any page in the platform.

<img src="top-nav-bar.png" alt="EdCast Top Navigation Bar">

#### My Learning Plan

In your learning plan, you can view:

- courses assigned to you
- courses you've enrolled in or followed

#### GitLab Learn Support Resources

This navgation bar links to the following support resources:

1. [GitLab Docs](https://docs.gitlab.com/)
1. [GitLab Support Ticket](https://support.gitlab.com/hc/en-us)
1. [GitLab Handbook](/about.gitlab.com/handbook/)
1. User Documentation and Trainings (on this page)

#### Search bar

Use the search bar to find new content and other learner profiles in the LXP. [Additional search strategies](/handbook/people-group/learning-and-development/gitlab-learn/user/#finding-new-learning-content) are outlined below.

#### Notifications

View notifications to see when new content is assigned to you, when users have commented on something you follow, and more.

#### Your Profile

Access your [GitLab Learn profile page](/handbook/people-group/learning-and-development/gitlab-learn/user/#setting-up-your-profile-page).


### Setting up your profile page

You can edit and customize your profile page from within the platform using the following steps.

If you'd rather watch a video of how to update your GitLab Learn profile, click [here](https://www.youtube.com/watch?v=WkO-5_QfyPE&feature=youtu.be) or watch the video below.

<iframe width="560" height="315" src="https://www.youtube.com/embed/WkO-5_QfyPE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

1. In the top navigation bar, locate your name and profile image. This will link to your profile page
1. On your profile, you can review your dashboard, content, learning plan, and more
1. In the top right corner of the screen, click `Edit Profile`
1. Confirm you are viewing the `Account Details` tab
1. From this page, you can edit and update the following information:
     - First and last name
     - Bio
     - Login information
     - Handle
     - Preferred language
     - Profile and banner image
     - Job title


### User privacy in GitLab Learn

Please review the following about user privacy when it comes to learning content in GitLab Learn:

1. Users can hide themselves from the Leaderboard if they don't wish to appear on this report. To do so, navigate to your profile, click `Edit Profile` and in the `Account Details` tab, check the box next to `Hide me from Leaderboards`
1. Bookmarked content is not visible by other users on GitLab Learn. You can access your own bookmarked courses by navigating to your profile page, clicking `Content` then choosing `Bookmarked` in the table on the left side of the screen.

### Understanding content in GitLab Learn

<iframe width="560" height="315" src="https://www.youtube.com/embed/pJzj4l3tG-4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Learning material is built and organized in the GitLab Learn LXP to help you follow a clear path. This is created using Smartcards, Pathways, and Journeys. More information on this content can be found on the [GitLab Learn contribution page](/handbook/people-group/learning-and-development/gitlab-learn/contribute) and in [this video training](https://youtu.be/pJzj4l3tG-4)

| Content Type | Purpose | Example |
| ----- | ----- | ----- |
| Journey | A collection of learning pathways | The Field Enablement team uses a journey to organize all content in the field certification. |
| Pathway | A single learning pathway | In the DIB certification, one required piece includes the reivew of a YouTube Video, reading 1 handbook page, and answering a short quiz. All 3 steps are contained in one pathway. |
| Smartcard | A single unit of learning content | One video, a 5 minute handbook read, a poll, and more. These are the basic units of learning content in the LXP and are used to build pathways and journeys. |

In addition, learing content and users are organized in the LXP to help you discover new learning material and follow learners interested in similar topics. The following strucutres organize content and users.

| Content Type | Purpose | Example |
| ----- | ----- | ----- |
| Carousel | Carousels are horizontal containers of related channels or content assets. | On the Discover page, you might see a carousel of featured DevOps pathways and smartcards |
| Channel | Channels are the principal way in which content (SmartCards, Pathways, and Journeys) are broadcasted to learners. | A series of SmartCards, Pathways, and Journeys on Agile Project Management |
| Group | Organize learners with a common job title, team, or interest | A group is organized for all GitLab team members to access the correct anti-harassment course | 



### Finding new learning content

There are many ways to search GitLab Learn for new learning content.

1. **Use the search bar.** This search feature will allow you to search by key word, username, or topic. This search bar is helpful if you're looking for material on a certain topic, or you're interested in seeing what your team members are learning.

1. **Follow Channels** Channels organize content based on topic. Find Channels you're interested in by searchin for a specific topic or expertise. You can follow these channels and be notified when new contnet is shared.

1. **Use the `More` option in the top navigation bar.** This waffle icon will allow you to sort through content based on users, groups, and more. If you'd like to learn more about the types of content in the LXP, reivew the [LXP contribution process](/handbook/people-group/learning-and-development/gitlab-learn/contribute)

Watch [this training](https://youtu.be/vGhtuY_gIEw) to review strategies for finding new learning content in GitLab Learn:

<iframe width="560" height="315" src="https://www.youtube.com/embed/vGhtuY_gIEw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Sharing your GitLab Learn Badges

When completing our pathways and journeys on the GitLab Learn platform, you will have opportunity to earn badges. 

For a video walkthrough on sharing your badge to LinkedIn watch the following video: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/zej2pLuwCok" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Reporting content in violation of the GitLab Code of Conduct

GitLab Learn users are encouraged to report content on the platform that is in violation of the [GitLab Code of Conduct](/community/contribute/code-of-conduct/). 

Steps for reporting GitLab Learn content:

1. To report content, click on the navigation symbol (3 vertical dots) in the SmartCard and choose the `Report It` option.
1. To report comments, click on the navigation symbol (3 vertical dots) to the right of the comment and choose the `Report It` option.

When reporting any content, you'll be asked if the content fits one of the following conditions. Please select the condition that is most applicable.
1. `Technical issues with opening the content`
1. `I think its fake, spam, or scam`
1. `I think its an inappropriate comment`

There is also an option to add more context around what is wrong with the content. Please feel encouraged to add any additional information in this section to help our team moderate.

From here, the GitLab L&D team will moderate and remove comments and content in violation of our CoC.

## Reporting content errors in GitLab Learn

Learners in the platform might come across errors like spelling mistakes, broken links, or information that is not up to date. Please help our team by reporting these errors by open an issue using the template `gitlab-learn-error-report` on the [lxp-contributions issue board](/gitlab-com/people-group/learning-development/lxp-contributions/-/boards)

## Frequently asked questions


### When will content in GitLab Learn be available for the entire GitLab team, customers, and community members?

The LXP implementation team is planning a phased rollout of the platform to multiple GitLab audiences. Please watch this page for future updates.

### Where is the content in the LXP stored?

Content in GitLab Learn is developed using a [handbook first](/handbook/people-group/learning-and-development/interactive-learning/) approach. All learning content is stored in the handbook. Teams at GitLab use additional tools like Articulate 360 and Rise to build interactive courses.

In addition to GitLab learning material stored in the handbook, GitLab team members will have access to off the shelf learning opportunity from other vendors and organizations, like LinkedIn Learning and WILL Learning.

### How can I contribute?

The contribution process to the LXP is being built, and you can follow updates in the [learning and development handbook](/handbook/people-group/learning-and-development/gitlab-learn/contribute/).

### How do I share a GitLab Learn badge externally?

Please review [this SmartCard](https://gitlab.edcast.com/insights/ECL-15365c36-f581-47fd-8c43-bf6d85e61656) to learn the process for how to share a GitLab Learn badge on LinkedIn.

### I can't mark a SmartCard as complete - what should I do?

Please review the steps for [submitting a support ticket](/handbook/people-group/learning-and-development/gitlab-learn/user/#support-options-for-gitlab-learn-users)

### I'm having trouble logging in or making a new account on GitLab Learn.

Please review the steps for [submitting a support ticket](/handbook/people-group/learning-and-development/gitlab-learn/user/#support-options-for-gitlab-learn-users).

### I can't see all the content in a Pathway or Journey on GitLab Learn.

Please note that some content in the Pathway is restricted to internal team members, specific audiences, or paid customers. If you believe you're having trouble accessing public content, please review the steps for [submitting a support ticket](/handbook/people-group/learning-and-development/gitlab-learn/user/#support-options-for-gitlab-learn-users).

### I found a mistake on GitLab Learn - how do I let someone know?

Thanks for helping us keep the content in GitLab Learn up to date! Please refer to the [reporting an error on GitLab Learn handbook section above](/handbook/people-group/learning-and-development/gitlab-learn/user/#reporting-content-errors-in-gitlab-learn) for instructions.

### I'm having trouble viewing a course. All I can see is a blank screen.

This could be caused by your pop-up blocker or chrome extension. Please try the following steps to access the course material:

1. Use the preferred Chrome browser
1. Open the course in an incognito window

## Have additional questions about GitLab Learn?

Questions from team members about the LXP can be directed to the Learning and Development team via the [#learninganddevelopment Slack channel](https://app.slack.com/client/T02592416/CMRAWQ97W). Community members should post on the [GitLab Community Forum](https://forum.gitlab.com/) or [submit a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000647759).


